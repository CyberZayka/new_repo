
class Table {

    renderTable() {
        const wrapper = document.querySelector('#wrapper');
        const table = document.createElement('table');
        table.classList.add('table-style');
        this.createTable(table);
        wrapper.append(table);
    }

    createTable(parentNode) {
        for(let i = 0; i < 10; i++) {
            const tr = document.createElement('tr');
            parentNode.append(tr);
            for(let j = 0; j < 10; j++) {
                const td = document.createElement('td');
                td.classList.add('td-style');
                tr.append(td);
            }
        }
    }

}

class Timer extends Table {
    constructor() {
        super();
        this.renderTime();
    }

    renderTime() {
        this.renderTable();
        this.countDown();
    }

    easyLevel = () => {
        const td = document.querySelectorAll('.td-style');
        const wrapper = document.querySelector('#wrapper');
        const timeValue = document.createElement('p');
        const playerScore = document.createElement('p');
        const pcScore = document.createElement('p');

        let timer;
        let num = 5;
        let player = 0;
        let pc = 0;

        wrapper.prepend(timeValue, playerScore, pcScore);

        count();

        function count() {
            let randomIndex = Math.floor(Math.random() * td.length);
            const table = document.querySelector('.table-style');

            const randomId = td[randomIndex];
            randomId.style.backgroundColor = 'blue';

            timeValue.innerHTML = `Time left: ${num}`;
            playerScore.innerHTML = `Player's score is: ${player}`;
            pcScore.innerHTML = `PC's score is: ${pc}`;
            num--;

            setTimeout(() => {
                if (randomId.style.backgroundColor === 'blue') {
                    randomId.style.backgroundColor = 'red';
                    pc++;
                }
            },1500)

            if (num < 0) {
                clearTimeout(timer);
                timeValue.remove();
                if (player > pc) alert('You win! Congratulations!');
                else if (pc > player) alert('PC win =( try later again');
                else alert('ooh my..that`s interesting try game again');
                playerScore.remove();
                pcScore.remove();
                const td = document.querySelectorAll('.td-style');
                td.forEach(item => {
                    item.style.backgroundColor = 'inherit';
                })
                const easy = document.querySelector('.easy');
                easy.checked = false;
            } else {
                timer = setTimeout(count, 1500);
            }

            table.addEventListener('click', (event) => {
                const clickedTd = event.target;

                if (clickedTd.style.backgroundColor === 'blue') {
                    clickedTd.style.backgroundColor = 'green';
                    player++;
                }
            })
        }
    }

    mediumLevel = () => {
        const td = document.querySelectorAll('.td-style');
        const wrapper = document.querySelector('#wrapper');
        const timeValue = document.createElement('p');
        const playerScore = document.createElement('p');
        const pcScore = document.createElement('p');

        let timer;
        let num = 5;
        let player = 0;
        let pc = 0;

        wrapper.prepend(timeValue, playerScore, pcScore);

        count();

        function count() {
            let randomIndex = Math.floor(Math.random() * td.length);
            const table = document.querySelector('.table-style');

            const randomId = td[randomIndex];
            randomId.style.backgroundColor = 'blue';

            timeValue.innerHTML = `Time left: ${num}`;
            playerScore.innerHTML = `Player's score is: ${player}`;
            pcScore.innerHTML = `PC's score is: ${pc}`;
            num--;

            setTimeout(() => {
                if (randomId.style.backgroundColor === 'blue') {
                    randomId.style.backgroundColor = 'red';
                    pc++;
                }
            },1000)

            if (num < 0) {
                clearTimeout(timer);
                timeValue.remove();
                if (player > pc) alert('You win! Congratulations!');
                else if (pc > player) alert('PC win =( try later again');
                else alert('ooh my..that`s interesting try game again');
                playerScore.remove();
                pcScore.remove();
                const td = document.querySelectorAll('.td-style');
                td.forEach(item => {
                    item.style.backgroundColor = 'inherit';
                })
                const medium = document.querySelector('.medium');
                medium.checked = false;
            } else {
                timer = setTimeout(count, 1000);
            }

            table.addEventListener('click', (event) => {
                const clickedTd = event.target;

                if (clickedTd.style.backgroundColor === 'blue') {
                    clickedTd.style.backgroundColor = 'green';
                    player++;
                }
            })
        }
    }

    hardLevel = () => {
        const td = document.querySelectorAll('.td-style');
        const wrapper = document.querySelector('#wrapper');
        const timeValue = document.createElement('p');
        const playerScore = document.createElement('p');
        const pcScore = document.createElement('p');

        let timer;
        let num = 5;
        let player = 0;
        let pc = 0;

        wrapper.prepend(timeValue, playerScore, pcScore);

        count();

        function count() {
            let randomIndex = Math.floor(Math.random() * td.length);
            const table = document.querySelector('.table-style');

            const randomId = td[randomIndex];
            randomId.style.backgroundColor = 'blue';

            timeValue.innerHTML = `Time left: ${num}`;
            playerScore.innerHTML = `Player's score is: ${player}`;
            pcScore.innerHTML = `PC's score is: ${pc}`;
            num--;

            setTimeout(() => {
                if (randomId.style.backgroundColor === 'blue') {
                    randomId.style.backgroundColor = 'red';
                    pc++;
                }
            },500)

            if (num < 0) {
                clearTimeout(timer);
                timeValue.remove();
                if (player > pc) alert('You win! Congratulations!');
                else if (pc > player) alert('PC win =( try later again');
                else alert('ooh my..that`s interesting try game again');
                playerScore.remove();
                pcScore.remove();
                const td = document.querySelectorAll('.td-style');
                td.forEach(item => {
                    item.style.backgroundColor = 'inherit';
                })
                const hard = document.querySelector('.hard');
                hard.checked = false;
            } else {
                timer = setTimeout(count, 500);
            }

            table.addEventListener('click', (event) => {
                const clickedTd = event.target;

                if (clickedTd.style.backgroundColor === 'blue') {
                    clickedTd.style.backgroundColor = 'green';
                    player++;
                }
            })
        }
    }

    countDown() {

        const wrapper = document.querySelector('#wrapper');
        const countDownBtn = document.createElement('button');
        const checkEasy = document.createElement('input');
        const checkMedium = document.createElement('input');
        const checkHard = document.createElement('input');
        const checkBox = document.createElement('div');
        const gameForm = document.createElement('div');

        checkEasy.type = 'checkbox';
        checkMedium.type = 'checkbox';
        checkHard.type = 'checkbox';

        checkEasy.classList.add('easy');
        checkMedium.classList.add('medium');
        checkHard.classList.add('hard');

        const easyLabel = document.createElement('label');
        easyLabel.innerHTML = 'Easy';
        easyLabel.append(checkEasy);

        const mediumLabel = document.createElement('label');
        mediumLabel.innerHTML = 'Medium';
        mediumLabel.append(checkMedium);

        const hardLabel = document.createElement('label');
        hardLabel.innerHTML = 'Hard';
        hardLabel.append(checkHard);

        const countValue = document.createElement('p');

        countDownBtn.innerText = 'Start';

        gameForm.classList.add('form-game-style');
        countDownBtn.classList.add('button-game-style');

        checkBox.append(easyLabel, mediumLabel, hardLabel);
        gameForm.append(checkBox, countDownBtn);
        wrapper.prepend(gameForm,countValue);

        checkEasy.onchange = () => {
            checkMedium.checked = false;
            checkHard.checked = false;
        }

        checkMedium.onchange = () => {
            checkEasy.checked = false;
            checkHard.checked = false;
        }

        checkHard.onchange = () => {
            checkEasy.checked = false;
            checkMedium.checked = false;
        }

        countDownBtn.addEventListener('click',() => {
            if (checkEasy.checked) {
                let timer;
                let num = 3;
                const that = this;

                count();
                function count() {
                    countValue.innerHTML = num;
                    num--;
                    if (num < 0) {
                        clearTimeout(timer);
                        that.easyLevel();
                        countValue.remove();
                    } else {
                        timer = setTimeout(count, 1000);
                    }
                }
            } else if (checkMedium.checked) {
                let timer;
                let num = 3;
                const that = this;

                count();
                function count() {
                    countValue.innerHTML = num;
                    num--;
                    if (num < 0) {
                        clearTimeout(timer);
                        that.mediumLevel();
                        countValue.remove();
                    } else {
                        timer = setTimeout(count, 1000);
                    }
                }
            } else if (checkHard.checked) {
                let timer;
                let num = 3;
                const that = this;

                count();
                function count() {
                    countValue.innerHTML = num;
                    num--;
                    if (num < 0) {
                        clearTimeout(timer);
                        that.hardLevel();
                        countValue.remove();
                    } else {
                        timer = setTimeout(count, 1000);
                    }
                }
            } else {
                alert('Choose a level of the game please');
            }
        })
    }

}


const test = new Timer();