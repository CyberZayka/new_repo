import {getPosts, addCard, getFirstAuthorName, getFirstAuthorMail, getId} from './requests.js'

document.addEventListener('DOMContentLoaded', buildFunc);

const API = 'https://jsonplaceholder.typicode.com';
const formCard = document.querySelector('.form-card');

class Posts {
    constructor({userId, title, body, name, id}) {
        this.userId = userId;
        this.title = title;
        this.body = body;
        this.name = name;
        this.id = id;

        //create DOM-elements in cards
        this.elements = {
            userId: document.createElement('h4'),
            title: document.createElement('h4'),
            body: document.createElement('p'),
            email: document.createElement('h5'),
        };
    }

    //function to get all id`s of posts
    async getId() {
        const resp = await fetch(`${API}/posts/${this.id}`);
        return resp.json().then(post => post.id);
    }

    //function to get all users name via usersId
    async getAuthorName() {
        const resp = await fetch(`${API}/users/${this.userId}`);
        return resp.json().then(user => user.name);
    }

    //function to get all users email via usersId
    async getAuthorMail() {
        const resp = await fetch(`${API}/users/${this.userId}`);
        return resp.json().then(user => user.email);
    }

    //function to render a card on the web page
    render(selector) {
        const targetElement = document.querySelector(selector);
        this.divElement = document.createElement('div');
        let {userId, email, title, body} = this.elements;

        this.divElement.classList.add('card');
        this.divElement.classList.add('cards-style');

        userId.classList.add('card-title');
        email.classList.add('card-subtitle');
        email.classList.add('mb-2');
        email.classList.add('text-muted');
        title.classList.add('card-title');
        body.classList.add('card-text');

        //get and write properties of card
        this.getAuthorName().then(name => userId.innerText = name);
        this.getAuthorMail().then(mail => email.innerText = mail);
        title.innerText = this.title;
        body.innerText = this.body;

        this.btnWrapper = document.createElement('div');
        this.btnWrapper.classList.add('btn-wrap-style');

        this.editBtn = document.createElement('button');
        this.deleteBtn = document.createElement('button');

        this.editBtn.classList.add('btn');
        this.editBtn.classList.add('btn-info');
        this.editBtn.classList.add('btn-style');

        this.deleteBtn.classList.add('btn');
        this.deleteBtn.classList.add('btn-danger');
        this.deleteBtn.classList.add('btn-style');

        this.editBtn.innerText = 'edit';
        this.deleteBtn.innerText = 'delete';

        this.editBtn.addEventListener('click', (event) => {
            this.getId().then(postId => {

                function toEditCard() {
                    const formSubmit = document.querySelector('#form-submit');
                    const addCard = document.querySelector('#add-card');

                    showHideForm();

                    formSubmit.addEventListener('click', (event) => {
                        event.preventDefault();

                        const formTitle = document.querySelector('#title');
                        const formText = document.querySelector('#text');

                        //check fields of inputs (on empty)
                        //if there is data in the fields, get data-object
                        if (formTitle.value !== '' && formText.value !== '') {
                            const formDataObject = getFormData(formTitle, formText);
                            requestToEditCard(formDataObject);

                            addCard.click();

                            title.innerText = formTitle.value;
                            body.innerText = formText.value;

                            //if there is no data-object, make border-color of fields red
                        } else {
                            if (formTitle.value === '') {
                                formTitle.style.borderColor = 'red';
                            }
                            if (formText.value === '') {
                                formText.style.borderColor = 'red';
                            }
                        }
                    });

                }

                function requestToEditCard(dataObjectToChange) {
                    //request to the server for change of card that was clicked
                    fetch(`${API}/posts/${postId}`, {
                        method: 'PUT',
                        body: JSON.stringify(dataObjectToChange),
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                        .then(response => {
                            if (response.ok) {
                                console.log(response);

                                alert('You have edited the card');
                            }
                        })
                }

                if (confirm('Are you sure you want to edit this card?')) {
                    toEditCard();
                }

            })
        })

        this.deleteBtn.addEventListener('click', (event) => {

            //get id of card that was clicked
            this.getId().then(postId => {

                if (confirm('Are you sure you want to delete this card?')) {

                    //request to delete on server a card that was clicked
                    fetch(`${API}/posts/${postId}`, {
                        method: 'DELETE',
                    })
                        .then(response => {
                            if (response.ok) {

                                //delete the dom-element of card that was clicked
                                event.target.closest('.card').remove();
                                alert('You have deleted the card');
                            }
                        })
                }
            })
        });

        this.btnWrapper.append(this.editBtn, this.deleteBtn);
        this.divElement.append(userId, email, title, body, this.btnWrapper);
        targetElement.append(this.divElement);
    }

    //function to create form for creating a new card
    createCard() {

        const formSubmit = document.querySelector('#form-submit');
        const addCardBtn = document.querySelector('#add-card');

        showHideForm();

        formSubmit.addEventListener('click', (event) => {
            event.preventDefault();

            const formTitle = document.querySelector('#title');
            const formText = document.querySelector('#text');

            //check fields of inputs (on empty)
            //if there is data in the fields, get data-object
            if (formTitle.value !== '' && formText.value !== '') {
                const formDataObject = getFormData(formTitle, formText);

                //send data-object to function for making post request to the server
                addCard(formDataObject)

                addCardBtn.click();
                //showHideForm();

                //if there is no data-object, make border-color of fields red
            } else {
                if (formTitle.value === '') {
                    formTitle.style.borderColor = 'red';
                }
                if (formText.value === '') {
                    formText.style.borderColor = 'red';
                }
            }
        });

    }

}


//main function is working when dom is loaded
//call function to get all posts
//initialize callback on 'add-card' button
function buildFunc() {
    getPosts()
        .then(data => data.forEach(item => {
            const post = new Posts(item);
            post.render("#main-wrapper");
            const addCard = document.querySelector('#add-card');
            addCard.addEventListener('click', new Posts(item).createCard)
        }));
}

//function to show or hide from for creating cards
function showHideForm() {

    if (formCard.classList.contains('dn')) {
        formCard.classList.replace('dn','db');
        document.body.style.backgroundColor = 'grey';
    } else {
        formCard.classList.replace('db', 'dn');
        document.body.style.backgroundColor = 'inherit';
    }

}

//function to get array of inputs of form and make object with data
function getFormData(...arrOfInputs) {
    const formData = {};

    arrOfInputs.forEach((input, index) => {
        formData[index] = input.value;
    })

    return formData;
}

//function to render created card on the web page
//the same function in the class, but it has functions from global scope
function renderAddedCard(objectCard) {

    const divElement = document.createElement('div');

    const userId = document.createElement('h4');
    const title = document.createElement('h4');
    const body = document.createElement('p');
    const email = document.createElement('h5');

    divElement.classList.add('card');
    divElement.classList.add('cards-style');

    userId.classList.add('card-title');
    email.classList.add('card-subtitle');
    email.classList.add('mb-2');
    email.classList.add('text-muted');
    title.classList.add('card-title');
    body.classList.add('card-text');

    getFirstAuthorName().then(name => userId.innerText = name);
    getFirstAuthorMail().then(mail => email.innerText = mail);
    title.innerText = objectCard[0];
    body.innerText = objectCard[1];

    const btnWrapper = document.createElement('div');
    btnWrapper.classList.add('btn-wrap-style');

    const editBtn = document.createElement('button');
    const deleteBtn = document.createElement('button');

    editBtn.classList.add('btn');
    editBtn.classList.add('btn-info');
    editBtn.classList.add('btn-style');

    deleteBtn.classList.add('btn');
    deleteBtn.classList.add('btn-danger');
    deleteBtn.classList.add('btn-style');

    editBtn.innerText = 'edit';
    deleteBtn.innerText = 'delete';

    editBtn.addEventListener('click', (event) => {
        getId().then(postId => {
            function toEditCard() {
                const formSubmit = document.querySelector('#form-submit');
                const addCard = document.querySelector('#add-card');

                showHideForm();

                formSubmit.addEventListener('click', (event) => {
                    event.preventDefault();

                    const formTitle = document.querySelector('#title');
                    const formText = document.querySelector('#text');

                    //check fields of inputs (on empty)
                    //if there is data in the fields, get data-object
                    if (formTitle.value !== '' && formText.value !== '') {
                        const formDataObject = getFormData(formTitle, formText);
                        requestToEditCard(formDataObject);

                        addCard.click();

                        title.innerText = formTitle.value;
                        body.innerText = formText.value;

                        //if there is no data-object, make border-color of fields red
                    } else {
                        if (formTitle.value === '') {
                            formTitle.style.borderColor = 'red';
                        }
                        if (formText.value === '') {
                            formText.style.borderColor = 'red';
                        }
                    }
                });

            }

            function requestToEditCard(dataObjectToChange) {
                //request to the server for change of card that was clicked
                fetch(`${API}/posts/${postId}`, {
                    method: 'PUT',
                    body: JSON.stringify(dataObjectToChange),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                    .then(response => {
                        if (response.ok) {
                            console.log(response);

                            alert('You have edited the card');
                        }
                    })
            }

            if (confirm('Are you sure you want to edit this card?')) {
                toEditCard();
            }
        })
    })

    deleteBtn.addEventListener('click', (event) => {

        //get id of card that was clicked
        getId().then(postId => {

            if (confirm('Are you sure you want to delete this card?')) {

                //request to delete on server a card that was clicked
                fetch(`${API}/posts/${postId}`, {
                    method: 'DELETE',
                })
                    .then(response => {
                        if (response.ok) {

                            //delete the dom-element of card that was clicked
                            event.target.closest('.card').remove();
                            alert('You have deleted the card');
                        }
                    })
            }
        })
    });

    btnWrapper.append(editBtn, deleteBtn);
    divElement.append(userId, email, title, body, btnWrapper);

    //get all cards dom-elements
    const cards = document.querySelectorAll('.cards-style');

    for (let i = 0; i < cards.length; i++) {

        //enter created card dom-element before first node
        if (i === 0) {
            cards[i].before(divElement);
        }
        break;
    }

}

export {renderAddedCard, API}

