import {renderAddedCard, API} from "./main.js";

//asynchronous function to get all posts from api
async function getPosts() {
    const response = await fetch(`${API}/posts`);
    return response.json()
}

//function to create post request to the server for creating a card
async function addCard(dataToSend) {
    await fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: JSON.stringify(dataToSend),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => response.json())
        .then(data => renderAddedCard(data))
}

//the same function in class
//but get only first user name
async function getFirstAuthorName() {
    const resp = await fetch(`${API}/users/1`);
    return resp.json().then(user => user.name);
}

//the same function in class
//but get only first user email
async function getFirstAuthorMail() {
    const resp = await fetch(`${API}/users/1`);
    return resp.json().then(user => user.email);
}

//the same function in class
//but get only first post id (for deleting card)
async function getId() {
    const resp = await fetch(`${API}/posts/1`);
    return resp.json().then(post => post.id);
}

export {getPosts, addCard, getFirstAuthorName, getFirstAuthorMail, getId}