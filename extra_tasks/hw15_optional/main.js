
const userNum = parseInt(prompt('Enter a value for factorial'));
//with loop
/*
function factorial(num) {

    let resultNum = 1;
    for (let i = 1; i <= num; i++) {
        resultNum *= i;
    }
    return resultNum;
}

alert(factorial(userNum));
 */
//with recursion

function factorial(num) {

   if (!isNaN(num)) {

       if (num === 1) return 1;
       return num * factorial(num - 1);

   } else {

       return 'something goes wrong!';

   }
}

alert(factorial(userNum));

