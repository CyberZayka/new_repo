document.addEventListener("DOMContentLoaded", createTable);

const wrapper = document.querySelector('#wrapper');

function createTable() {
    const table = document.createElement('table');
    table.classList.add('table-style');

    for(let i = 0; i < 30; i++) {
        const tr = document.createElement('tr');
        table.append(tr);

        for(let j = 0; j < 30; j++) {
            const td = document.createElement('td');
            td.classList.add('td-style');
            tr.append(td);
        }
    }
    wrapper.append(table);
    handleClickToTable();
}


function handleClickToTable() {
    const table = document.querySelector('.table-style');

    document.body.addEventListener('click', (event) => {
        if(event.currentTarget !== table) {
            document.querySelectorAll('td').forEach(td => {
                if(td.classList.contains('td-background-color')) {
                    td.classList.remove('td-background-color')
                } else {
                    td.classList.add('td-background-color');
                }

            })
            
        } else {
            return;
        }
    })

    table.addEventListener('click', (event) => {
        event.stopPropagation();
        const clicked = event.target;
        
        if(clicked.classList.contains('td-background-color') && clicked.tagName !== 'TABLE') {
            clicked.classList.remove('td-background-color');
        } else if(!clicked.classList.contains('td-background-color') && clicked.tagName !== 'TABLE') {
            clicked.classList.add('td-background-color');
        }

    })
}