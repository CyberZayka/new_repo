
const prevBtn = document.querySelector('.prev');
const nextBtn = document.querySelector('.next');

const arrOfImg = document.querySelectorAll('.slide-image');

const listBtn = document.querySelectorAll('.small-btn__item');

let i = 0;
let j = 0;

nextBtn.addEventListener('click', function () {
    arrOfImg[i].style.display = 'none';
    listBtn[j].style.backgroundColor = 'gray';
    i++;
    j++;

    if (i === arrOfImg.length && j === listBtn.length) {
        i = 0;
        j = 0;
    }

    listBtn[j].style.backgroundColor = '#4c4c4c';
    arrOfImg[i].style.display = 'block';
})

prevBtn.addEventListener('click', function () {
    arrOfImg[i].style.display = 'none';
    listBtn[j].style.backgroundColor = 'gray';
    i--;
    j--;

    if (i < 0 && j < 0) {
        i = arrOfImg.length - 1;
        j = listBtn.length - 1;
    }

    arrOfImg[i].style.display = 'block';
    listBtn[j].style.backgroundColor = '#4c4c4c';
})

//вот как это сделать, чтобы работало?)
/*
for (let listItem of listBtn) {
    listItem.addEventListener('click', function (event) {

        const dataList = event.target.getAttribute('data-list');
        const images = document.getElementsByClassName('slide-image');

        for (let j = 0; j < images.length; j++) {
            if (j == dataList) {
                images[j].style.display = 'block';
            } else {
                images[j].style.display = 'none';
            }
        }


        for (let i = 0; i < listBtn.length; i++) {
            listBtn[i].className = listBtn[i].className.replace(' list-active', '');
        }
        event.currentTarget.className += ' list-active';

    })
}
*/



