const drawBtn = document.querySelector('#draw-circle');
const wrapper = document.querySelector('#wrapper');
const btnWrap = document.createElement('div');
const input = document.createElement('input');

input.placeholder = 'Diameter of circles';
btnWrap.classList.add('btnWrap');

const colors = ['#f43c0e', ' #f4bc0e ', ' #6bf40e ', ' #0ef4d7 ', ' #0e8ff4 ', ' #aa0ef4 ', ' #f40eca ', ' #0d010b ', ' #868285 ', ' #613459 '];

const handleClickToCircle = (event) => {
    if(event.target.classList.contains('circle')) {
        event.target.remove();
    }
}

const createCircles = (diameter) => {
    for(let i = 0; i < 10; i++) {
        const circleBox = document.createElement('div');
        circleBox.classList.add('circle-box');
        wrapper.append(circleBox);
        for(let j = 0; j < 10; j++) {
            const circle = document.createElement('div');
            circle.style.width = `${diameter}px`;
            circle.style.height = `${diameter}px`;
            circle.style.backgroundColor = colors[Math.floor(Math.random() * 10)];
            circle.classList.add('circle');
            circleBox.append(circle);
        }
    }
    const btnWrap = document.querySelector('.btnWrap');
    btnWrap.remove();
    wrapper.addEventListener('click', handleClickToCircle);
}

const drawCircle = () => {
    drawBtn.innerText = 'Нарисовать';

    btnWrap.append(input, drawBtn)
    wrapper.append(btnWrap);

    drawBtn.removeEventListener('click', drawCircle);

    drawBtn.addEventListener('click', () => {
        const value = parseInt(input.value);
        if(value !== '' && !isNaN(value)) {
            createCircles(input.value);     
        } else {
            input.style.borderColor = 'red';
        }
    })
}

drawBtn.addEventListener('click', drawCircle);

