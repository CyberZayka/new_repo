
const {src, dest, series, parallel, watch, gulp} = require('gulp');
const sass = require('gulp-sass');
const clean = require('gulp-clean');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');

sass.compiler = require('node-sass');
const postcss = require('gulp-postcss');
const uncss = require('postcss-uncss');

const browserSync = require('browser-sync').create();

function copyHtml() {
    return src('./src/index.html')
        .pipe(dest('./dist'))
        .pipe(browserSync.reload({ stream: true }));
}

function copyCss() {
    return src('./src/scss/**')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(postcss([
            uncss({html: ['./dist/index.html']})]))
        .pipe(sourcemaps.init())
        .pipe(autoprefixer())
        .pipe(concat('all.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(dest('./dist/css'))
        .pipe(browserSync.stream());
}

function copyJs() {
    return src('./src/js/**')
        .pipe(dest('./dist/js'))
        .pipe(browserSync.reload({ stream: true }));
}

function copyImg() {
    return src('./src/img/**')
        .pipe(dest('./dist/img'))
}

function cleanDist() {

    return src('./dist', {read: false})
        .pipe(clean())

}

function dev() {
    browserSync.init({
        server: './dist'
    });

    watch(['./src/index.html'], copyHtml);
    watch(['./src/scss/**'], copyCss);
    watch(['./src/js/**'], copyJs);
}

const build = series(
    cleanDist,
    parallel(
        series(copyHtml, copyCss),
        copyImg,
        copyJs
    )
);

exports.html = copyHtml;
exports.css = copyCss;
exports.js = copyJs;
exports.img = copyImg;
exports.clean = cleanDist;
exports.build = build;
exports.default = series(build, dev);