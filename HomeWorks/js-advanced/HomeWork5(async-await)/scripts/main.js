
document.body.style.width = '100%';

const div = document.createElement('div');
div.style.width = '100%';
div.style.height = '666px';
div.style.display = 'flex';
div.style.justifyContent = 'center';
div.style.alignItems = 'center';
div.style.flexDirection = 'column';
document.body.append(div);

const btn = document.createElement('button');
btn.style.width = '150px';
btn.style.height = '85px';
btn.style.backgroundColor = 'grey';
btn.style.color = 'white';
div.append(btn);
btn.innerText = 'Find Geo via IP';

btn.addEventListener('click', sendRequest);

const requestURL = 'https://api.ipify.org/?format=json';
const requestURLTwo = 'http://ip-api.com';

async function sendRequest() {
    try {

        const response = await fetch(requestURL);
        const data = await response.json();
        const reqResult = `${requestURLTwo}/json/${data.ip}`;

        const responseTwo = await fetch(reqResult);
        const resultData = await responseTwo.text();

        const text = document.createElement('p');
        text.innerText = resultData;
        div.append(text);

    } catch (err) {
        console.error(err);
    }

}

