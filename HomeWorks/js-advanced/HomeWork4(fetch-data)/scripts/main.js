
document.body.style.width = '100%';

const div = document.createElement('div');
div.style.width = '100%';
div.style.height = '666px';
div.style.display = 'flex';
div.style.justifyContent = 'center';
div.style.alignItems = 'center';
div.style.flexDirection = 'column';
document.body.append(div);

const btn = document.createElement('button');
btn.style.width = '150px';
btn.style.height = '85px';
btn.style.backgroundColor = 'grey';
btn.style.color = 'white';
div.append(btn);
btn.innerText = 'Find Geo via IP';

btn.addEventListener('click', sendRequest);

const requestURL = 'https://api.ipify.org/?format=json';
const requestURLTwo = 'http://ip-api.com';

function sendRequest() {
    return fetch(requestURL)
        .then(response => {
            return response.json();
        })
        .then(data => {
            const reqResult = `${requestURLTwo}/json/${data.ip}`;
            return fetch(reqResult)
                .then(responseTwo => {
                    return responseTwo.text();
                })
                .then(geo => {

                    const text = document.createElement('p');
                    text.innerText = geo;
                    div.append(text);

                })

        })
        .catch(error => {
            console.error(error);
        })
}

