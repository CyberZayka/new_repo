const cities = ['Kiev', 'Kharkiv', 'Odessa', 'Lviv', 'Dnipro']

const renderList = (list, parentElement = document.body) => {

    const listString = list.map(city => {
        return `<li>${city}</li>`
    }).join(" ")

    parentElement.insertAdjacentHTML("beforeend", `<ul>${listString}</ul>`)

}

renderList(cities)