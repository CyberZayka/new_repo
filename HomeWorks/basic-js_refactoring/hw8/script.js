

document.addEventListener("DOMContentLoaded", () => {
    const fieldWrap = document.querySelector("#root")
    fieldWrap.classList.add("fieldWrapper")

    showInputPrice(fieldWrap)
})


function showInputPrice(fieldWrap = document.body) {
    const inputBox = document.createElement("div")
    inputBox.classList.add("inputBox")

    const input = document.createElement("input")
    input.classList.add("baseClass")

    const spanPrice = document.createElement("span")
    spanPrice.textContent = "Price: "
    spanPrice.classList.add("spanPrice")

    inputBox.append(spanPrice, input)
    fieldWrap.append(inputBox)

    myInputListeners(input)
}


function myInputListeners(input, span) {
    input.addEventListener("focus", () => {
        input.classList.add("inputActive")
    })
    input.addEventListener("blur", () => {
        input.classList.remove("inputActive")
        getDataFromInput(input)
    })
    span.addEventListener("click", () => {
        input.value = ""
        document.querySelector(".wrapResult").remove()
    })
}

function getDataFromInput(input) {
    const value = Number(input.value)

    const checkErrorMsg = document.querySelector(".errorMessage")
    const checkValidMsg = document.querySelector(".wrapResult")

    if (checkErrorMsg) {
        checkErrorMsg.remove()
        input.classList.remove("inputError")
    } else if (checkValidMsg) {
        checkValidMsg.remove()
    }

    if (value > 0) {
        validForm(value, input)
    } else if (value < 0 || Number.isNaN(value)) {
        invalidForm(input)
    } else if (value === 0) {
        input.value = ""
    }

}

function invalidForm(input) {
    input.classList.add("inputError")

    const inputBox = document.querySelector(".inputBox")

    const checkErrorMsg = document.querySelector(".errorMessage")
    const checkValidMsg = document.querySelector(".wrapResult")

    checkValidMsg ? checkValidMsg.remove() : null

    if (!checkErrorMsg) {
        const errorMsg = document.createElement("span")
        errorMsg.textContent = "Please enter correct price"
        errorMsg.classList.add("errorMessage")
        inputBox.after(errorMsg)
    }
}

function validForm(price, input) {
    const fieldWrap = document.querySelector("#root")

    input.classList.contains("inputError") ? input.classList.remove("inputError") : null

    const wrapResult = document.createElement("span")
    wrapResult.classList.add("wrapResult")

    const resultPrice = document.createElement("a")
    resultPrice.href = "#"
    resultPrice.textContent = "X";
    resultPrice.classList.add("resultSpan")

    wrapResult.textContent = price
    wrapResult.append(resultPrice)

    const checkErrorMsg = document.querySelector(".errorMessage")
    const checkValidMsg = document.querySelector(".wrapResult")

    if (checkErrorMsg) {
        checkErrorMsg.remove()
        fieldWrap.prepend(wrapResult)
    } else if (checkValidMsg) {
        checkValidMsg.remove()
        fieldWrap.prepend(wrapResult)
    } else {
        fieldWrap.prepend(wrapResult)
    }

    myInputListeners(input, resultPrice)
}





