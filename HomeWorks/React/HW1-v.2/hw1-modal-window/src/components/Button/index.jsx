import React, {Component} from 'react';
import styled from "styled-components";

class Button extends Component {

    render() {
        return (
        <StyledBtn
            disabled={this.props.disabled}
            primary={this.props.primary}
            primaryModal={this.props.primaryModal}
            bgColorDeleting={this.props.bgColorDeleting}
            bgColorSaving={this.props.bgColorSaving}
            onClick={this.props.onClick}>
            {this.props.text}
        </StyledBtn>
        )
    }
}

const StyledBtn = styled.button`
  background: ${props => props.primary ? "palevioletred" : "white"};
  color: ${props => props.primary ? "white" : "palevioletred"};
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid palevioletred;
  border-radius: 3px;
  
  ${onclick}

  background: ${props => props.bgColorDeleting && "#b3382c"};
  
  background: ${props => props.bgColorSaving && "green"};

  color: ${props => props.primaryModal && "white"};
  margin: ${props => props.primaryModal && "0 1em"};
  width: ${props => props.primaryModal && "101px"};
  height: ${props => props.primaryModal && "41px"};
  border: ${props => props.primaryModal && "none"};
`;

export default Button;