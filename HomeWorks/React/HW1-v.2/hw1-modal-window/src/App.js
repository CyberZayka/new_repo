import React, {Component} from 'react';

import Button from "./components/Button";
import Modal from "./components/Modal";

const firstBtnText = "Save product"
const secondBtnText = "Delete product"

//id for parent block element to hide modal
const id = 1111;

class App extends Component {
    state = {
        isOpenModal: false,
        isDeletingOrder: false,
        isSavingOrder: false,
        modalHeaderTitle: null,
        modalBodyTitleFirstParagraph: null,
        modalBodyTitleSecondParagraph: null
    }

    changeStateModalUp = () => {
        this.setState({
            isOpenModal: true
        })
    }

    changeStateModalDown = () => {
        this.setState({
            isOpenModal: false
        })
    }

    //func for "ok" button in modal window. That was created for adding or deleting product
    makeOrder = () => {
        const {isDeletingOrder, isSavingOrder} = this.state

        this.changeStateModalDown()

        if (isDeletingOrder) {
            alert("You have successfully deleted this product")
            this.clearOrder()
        }
        if (isSavingOrder) {
            alert("Congratulations! You have added this product to the cart");
            this.clearOrder()
        }
    }

    cancelOrder = () => {
        this.changeStateModalDown()
        this.clearOrder()
    }

    clearOrder = () => {
        this.setState({
            isSavingOrder: false,
            isDeletingOrder: false
        })
    }

    showModalForDelete = () => {
        this.changeStateModalUp()

        this.setState({
            isDeletingOrder: true,
            modalHeaderTitle: "Do you want to delete this file?",
            modalBodyTitleFirstParagraph: "Once you delete this file, it won`t be possible to undo this action.",
            modalBodyTitleSecondParagraph: "Are you sure you want to delete it?"
        })
    }

    showModalForSave = () => {
        this.changeStateModalUp()

        this.setState({
            isSavingOrder: true,
            modalHeaderTitle: "Do you want to add this item to the cart?",
            modalBodyTitleFirstParagraph: "Saving product item to the cart.",
            modalBodyTitleSecondParagraph: "Are you sure you want to add it?"
        })
    }

    hideModal = (event) => {
        const {isOpenModal} = this.state
        const strId = id.toString()

        if (isOpenModal && event.target.id === strId) {
            this.cancelOrder()
        }
    }

    render() {
        const {isOpenModal,
            modalHeaderTitle,
            modalBodyTitleFirstParagraph,
            modalBodyTitleSecondParagraph,
            isSavingOrder,
            isDeletingOrder
        } = this.state;

        return (
            <div onClick={this.hideModal}>
                <Button disabled={isOpenModal} text={firstBtnText} onClick={this.showModalForSave} />
                <Button disabled={isOpenModal} primary text={secondBtnText} onClick={this.showModalForDelete} />

                {isOpenModal && <Modal headerTitle={modalHeaderTitle}
                                       bodyTitle={modalBodyTitleFirstParagraph}
                                       bodyText={modalBodyTitleSecondParagraph}
                                       id={id}
                                       isOpenModal={isOpenModal}
                                       bgColorDeleting={isDeletingOrder}
                                       bgColorSaving={isSavingOrder}
                                       makeOrder={this.makeOrder}
                                       cancelOrder={this.cancelOrder}
                />
                }
            </div>
        );
    }
}

export default App;
