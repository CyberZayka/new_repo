import React, {PureComponent, memo} from 'react';
import PropTypes from 'prop-types'

class Button extends PureComponent {
    render() {
        const { onClickHandler, text, icon } = this.props;
        return (
            <button onClick={onClickHandler}
                    className='card-add-to-btn'><span>{text}</span> {icon}</button>
        );
    }
}

export default memo(Button);

Button.propTypes = {
    onClickHandler: PropTypes.func,
    text: PropTypes.string
};