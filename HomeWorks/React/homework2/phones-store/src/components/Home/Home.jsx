import React, {PureComponent, memo} from "react";
import "./styles.scss";
import PhonesList from "../PhonesSection/PhonesList";

class Home extends PureComponent {
   state = {
      listOfPhones: [],
   };

   componentDidMount() {
      fetch('phones.json')
          .then(response => response.json())
          .then(data => {
             this.setState({listOfPhones: data})
          })
   }

   render() {
      const {listOfPhones} = this.state;

      return (
          <>
             <PhonesList collection={listOfPhones}/>
          </>
      );
   }
}

export default memo(Home);
