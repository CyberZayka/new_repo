import React, {PureComponent, memo} from 'react';
import PropTypes from 'prop-types';
import Button from "../Button/Button";
import Icon from "../Icon/Icon"
import './styles.scss'

class Phone extends PureComponent {
   state = {
      starColor: "lightgrey"
   };

   componentDidMount() {
      const {id} = this.props

      const isFav = localStorage.getItem("isFav")

      if(isFav) {
         const isFavoriteArray = JSON.parse(isFav)
         isFavoriteArray.forEach(fav => {
            if(+fav === id) this.setState({starColor: "gold"})
         })
      }
   }

   drawStar = () => {
      const {starColor} = this.state
      const {addToFavorites} = this.props
      starColor === "lightgrey" ? this.setState({starColor: "gold"}) : this.setState({starColor: "lightgrey"})
      addToFavorites()
   }

   render() {
      const cartIcon = <Icon type="cart" color="white" classes="svg-inline--fa fa-shopping-cart fa-w-18 cart-size" />
      const {imgUrl, name, price, color, addToCart} = this.props;
      return (
         <div className="singleCard">
            <img src={imgUrl} alt="img"/>
             
            <div className="card-name-box">
               <span className="card-name">{name}</span>
               <Icon type="star" classes="star-size" color={this.state.starColor} onClick={this.drawStar} />
            </div>

            <div className="price-box">
               <span className="card-price">Price: <span>{price} UAH</span></span>
               <span className="card-price">Color: {color} <div style={{backgroundColor: color}} className="color-box" ></div></span>
            </div>

            <Button text="Add to cart" icon={cartIcon} onClickHandler={addToCart}/>
         </div>
      );
   }
}

export default memo(Phone);

Phone.propTypes = {
   imgUrl: PropTypes.string,
   name: PropTypes.string,
   price: PropTypes.number,
   color: PropTypes.string,
};