import React, {PureComponent, memo} from 'react';
import Phone from "./Phone";
import PropTypes from 'prop-types'
import Modal from "../Modal/Modal";

class PhonesList extends PureComponent {
   state = {
      open: false,
      currentId: null,
      cart: [],
   };

   addToCart() {
      const {cart, currentId} = this.state;
      this.setState({cart: [...cart, currentId]}, () => {
         localStorage.setItem('cart', JSON.stringify(this.state.cart))
      });
   }

   addToFavorites(product_id) {
     
      const isFav = localStorage.getItem("isFav")

        if(isFav) {
            const isFavoriteArray = JSON.parse(isFav)

            const index = isFavoriteArray.indexOf(product_id)
            if(index === -1) {
                localStorage.setItem("isFav", JSON.stringify([...isFavoriteArray, product_id]))
            } else {
                isFavoriteArray.splice(index, 1)
                isFavoriteArray.length === 0 ? localStorage.removeItem("isFav") : localStorage.setItem("isFav", JSON.stringify(isFavoriteArray))
            }

        } else {
            localStorage.setItem("isFav", JSON.stringify([product_id]))
        }
   }

   modalState(forceValue) {
      this.setState({open: forceValue ? forceValue : !this.state.open})
   }

   setCurrentId(id) {
      this.setState({currentId: id})
   }

   modalOpen() {
      this.setState({open: !this.state.open});
   }

   render() {
      const {collection} = this.props;
      const {open} = this.state;

      const cardsList = collection.map(card => {
         return (
             <Phone
                 key={card.product_id}
                 className="card"
                 imgUrl={card.avatar}
                 name={card.model}
                 price={card.price}
                 color={card.color}
                 id={card.product_id}
                 addToCart={() => {
                    this.setCurrentId(card.product_id);
                    this.modalOpen();
                 }}
                 addToFavorites={() => {
                    this.addToFavorites(card.product_id);
                 }}
             />
         )
      });

      return (
          <>
             <div className="cards-wrapper container">
                {collection.length > 0 && cardsList}
             </div>
             {open && (
                 <>
                    <Modal
                        toggleModal={() => this.modalState(false)}
                        actions={
                           <>
                              <button onClick={() => {
                                 this.addToCart();
                                 this.modalState(false);
                              }}
                                      className="close__modal-wrapper">
                                 Yes
                              </button>

                              <button
                                  onClick={() => this.modalState(false)}
                                  className="close__modal-wrapper">
                                 No
                              </button>
                           </>
                        }
                    />
                 </>
             )}
          </>
      );
   }
}

export default memo(PhonesList);

PhonesList.propTypes = {
   collection: PropTypes.array,
};
