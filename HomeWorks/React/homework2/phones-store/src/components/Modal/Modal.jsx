import React, {PureComponent, memo} from 'react';
import './Modals.scss'
import PropTypes from 'prop-types'

class Modal extends PureComponent {

   render() {
      const {actions, toggleModal} = this.props;
      return (
          <>
             <div className="modal-wrapper">
                <div className="modal-bg-wrapper"
                     onClick={toggleModal}/>
                <div className="modal-inner">
                   <div className="modal login">
                      <p className="modal__header-text">
                         Add to Cart?
                      </p>
                      <p className="modal__main-text">
                         {actions}
                      </p>
                   </div>
                </div>
             </div>
          </>
      );
   }
}

export default memo(Modal);

Modal.propTypes = {
   btn: PropTypes.object
};
