import React, { PureComponent, memo } from "react";
import "./Header.scss";

class HeaderLink extends PureComponent {
  
  render() {
    return (
      <nav className="header__nav">
          <ul className="header__link-list">
              <li><a href="#" className="header__link">Home</a></li>
              <li><a href="#" className="header__link">Favorites</a></li>
              <li><a href="#" className="header__link">Cart</a></li>
          </ul>
      </nav>
    );
  }
}

export default memo(HeaderLink);