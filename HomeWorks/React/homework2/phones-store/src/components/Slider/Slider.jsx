import React, { PureComponent, memo } from "react";
import "./styles.scss";
import ImageSlide from "./ImageSlide";


const images = [
    "../../images/apple.jpg",
    "../../images/huawei.jpg",
    "../../images/motorola.jpg",
    "../../images/nokia.jpg",
    "../../images/oppo.jpg",
    "../../images/poco.jpg",
    "../../images/samsung.jpg",
    "../../images/vivo.jpg",
    "../../images/xiaomi.jpg",
    "../../images/zte.jpg"
]

class Slider extends PureComponent {
  
  render() {

    return (
    <div style={{width:"100%"}}>
        <div className="images-wrapper">
            {
                images.map((image, index) => {
                    return <ImageSlide srcUrl={image} key={index} />          
                })
                
            }
        </div>
    </div>
    );
  }
}

export default memo(Slider);