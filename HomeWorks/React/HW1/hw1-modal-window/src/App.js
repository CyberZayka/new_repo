import React, {Component} from 'react';
import Button from "./components/Button.js";
import Modal from "./components/Modal/Modal";
import * as ReactDOM from "react-dom";

class App extends Component {

    state = {
        isOpen: false,
        headerModalText: null,
        bodyModalText:null,
        headerColor: null,
        bodyColor: null,
        btnColor: null,
        textColor: null,
        mainBgColor: null,
        closeButton: false,
        backgroundColor: null,
        backgroundPosition: null,
        backgroundHeight: null,
        backgroundWidth: null
    }

    //func to change state of modal window
    changeState = () => {
        this.setState({
            isOpen: !this.state.isOpen,
            backgroundColor: null,
            backgroundPosition: null,
            backgroundHeight: null,
            backgroundWidth: null});
    }

    //func to show first modal window
    showModal = () => {
        //open or close modal window when button was clicked
        this.changeState();

        //setup state properties in first modal window
        this.setState({
            headerModalText: 'Do you want to delete this file?',
            bodyModalText:
                'Once you delete this file, it won’t be possible to undo this action. \n' +
                'Are you sure you want to delete it?',
            headerColor: '#d44637',
            bodyColor: '#e74c3c',
            btnColor: '#b3382c',
            textColor: '#ffffff',

            backgroundColor: 'lightgrey',
            backgroundPosition: 'absolute',
            backgroundHeight: '100%',
            backgroundWidth: '100%'})
    }

    //func to show second modal window
    showSecondModal = () => {
        //open or close modal window when button was clicked
        this.changeState();

        //setup state properties in first modal window
        this.setState({
            headerModalText: 'Congratulations, dude!!!!',
            bodyModalText:
                'You have been hacked =)',
            headerColor: '#029d1d',
            bodyColor: '#2ddd76',
            btnColor: '#0a632f',
            textColor: '#011308',

            backgroundColor: 'lightgrey',
            backgroundPosition: 'absolute',
            backgroundHeight: '100%',
            backgroundWidth: '100%'})
    }

    handleClickOutside = (event) => {

        if (event.target.tagName !== 'BUTTON') {
            this.changeState();
        }
    }

    render() {

    //properties (state) of modal window
    const {isOpen, headerModalText, bodyModalText, headerColor, bodyColor, btnColor, textColor} = this.state;

    //properties of button
    const firstBtnTitle = "Open first modal";
    const secondBtnTitle = "Open second modal";
    const firstBgColor = "blue";
    const secondBgColor = "grey";

    const modalWindow =
        <Modal
        headerText={headerModalText}
        bodyText={bodyModalText}
        headerColor={headerColor}
        bodyColor={bodyColor}
        btnColor={btnColor}
        textColor={textColor}
        isOpen={isOpen}
        changeState={this.changeState}
        />;

    return (
        <div onClick={this.handleClickOutside} style={{
            backgroundColor: this.state.backgroundColor,
            position: this.state.backgroundPosition,
            height: this.state.backgroundHeight,
            width: this.state.backgroundWidth
        }}>
            <Button text={firstBtnTitle} backgroundColor={firstBgColor} actions={this.showModal} isOpen={isOpen} />
            <Button text={secondBtnTitle} backgroundColor={secondBgColor} actions={this.showSecondModal} isOpen={isOpen} />

            {isOpen && modalWindow}

        </div>
    );
  }

}

export default App;
