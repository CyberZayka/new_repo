import React, {Component} from "react";

class Button extends Component {

    render() {
        const {text, backgroundColor, actions} = this.props;

        return (
            <button type="button" className="btn btn-lg" style={{backgroundColor}} onClick={actions}>{text}</button>
        )
    }
}

export default Button;


