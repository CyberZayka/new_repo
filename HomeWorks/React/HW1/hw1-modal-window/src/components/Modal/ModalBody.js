import React, {Component} from "react";

class ModalBody extends Component {
    render() {

        const {topic, color, textColor} = this.props;

        return (
            <div className="modal-body" style={{backgroundColor: color}}>
                <p style={{color: textColor}}>{topic}</p>
            </div>
        )
    }
}

export default ModalBody;