import React, {Component} from "react";
import ModalHeader from "./ModalHeader.js";
import ModalBody from "./ModalBody.js";
import ModalFooter from "./ModalFooter.js";
import App from "../../App";

class Modal extends Component {

    render() {
        const {headerText, bodyText, headerColor, bodyColor, btnColor, textColor, isOpen, changeState} = this.props;

        return (
            <div className="modal-container">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <ModalHeader title={headerText} color={headerColor} textColor={textColor} changeState={changeState} />
                        <ModalBody topic={bodyText} color={bodyColor} textColor={textColor} />
                        <ModalFooter color={bodyColor} btnColor={btnColor} isOpen={isOpen} changeState={changeState} />
                    </div>
                </div>
            </div>
        )
    }
}

export default Modal;