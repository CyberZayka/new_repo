import React, {Component} from "react";
import Button from "../Button.js";
import * as ReactDOM from "react-dom";

class ModalFooter extends Component {

    sayOk = (event) => {

        console.log(event.target);

        this.props.changeState();
        alert('You have approved form');
    }

    sayCancel = () => {
        this.props.changeState();
        alert('You have canceled form!');
    }


    render() {
        const firstText = "ok";
        const secondText = "cancel";
        const firstBgColor = "green";
        const secondBgColor = "red";

        const {btnColor, color} = this.props;

        return (
            <div className="modal-footer" style={{backgroundColor: color}}>
                <Button text={firstText} backgroundColor={firstBgColor} btnColor={btnColor} actions={this.sayOk} />
                <Button text={secondText} backgroundColor={secondBgColor} btnColor={btnColor} actions={this.sayCancel} />
            </div>
        )
    }
}

export default ModalFooter;