import React, {Component} from "react";
import Button from "../Button.js";

class ModalHeader extends Component {

    render() {

        const {title, color, textColor, changeState} = this.props;
        const btnCloseText = <p>&times;</p>

        return (
            <div className="modal-header" style={{backgroundColor: color}}>
                <h5 className="modal-title" style={{color: textColor}}>{title}</h5>
                <Button text={btnCloseText} actions={changeState} />
            </div>
        )
    }
}

export default ModalHeader;