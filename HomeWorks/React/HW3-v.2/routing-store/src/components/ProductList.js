import React, {useEffect, useState} from 'react';

import Modal from "./Modal";
import Phone from "./Phone";
import Icon from "./Icon";

const ProductList = (props) => {

    const {
        modalTitle,
        modalBody,
        isInHome,
        isInCart
    } = props;

    const [allSavedPhonesId, setAllSavedPhonesId] = useState([]);
    const [isModalOpen, setIsOpenModal] = useState(false);
    const [starColor, setStarColor] = useState('lightgrey');
    const [cartId, setCartId] = useState(null);

    const favoriteStarIcon = <Icon type="star" classes="star-style" color={starColor} onClick={saveFavoriteProductToStorage} />
    const addToCartIcon = <Icon type="cart" classes="cart-style" color="white" />
    const cancelCartIcon = <Icon type='cancel' classes='cancel-style' />

    //get data from localstorage 'favorite' in order to compare the current id of the star with the one saved in the storage
    useEffect(() => {

        const savedItemsArray = localStorage.getItem('favorite');

        if (savedItemsArray) {
            const parsedArray = JSON.parse(savedItemsArray);
            setAllSavedPhonesId(parsedArray)
        }


    }, [])

    //comparing id`s. if there is id in localstorage is the same with active id, then shine the star
    useEffect(() => {
        allSavedPhonesId.map(id => {
            if (id === props.product_id) setStarColor('gold')
            return allSavedPhonesId;
        })
    },[allSavedPhonesId, props.product_id])

    const showHideModal = () => {
        setIsOpenModal(!isModalOpen);
    }

    //func to save or delete a phone to/from localstorage, when a star was clicked, and shine this star
    function saveFavoriteProductToStorage() {

        const {product_id} = props;

        const getItem = localStorage.getItem('favorite');

        if (getItem) {
            const parsedItem = JSON.parse(getItem)

            const foundId = parsedItem.find(id => id === product_id)

            if (foundId) {
                setStarColor('lightgrey');
                const foundIndex = parsedItem.findIndex(indexId => indexId === product_id)
                parsedItem.splice(foundIndex, 1);
            } else {
                setStarColor('gold');
                parsedItem.push(product_id);
            }

            localStorage.setItem('favorite', JSON.stringify(parsedItem))
        } else {
            setStarColor('gold')
            localStorage.setItem('favorite', JSON.stringify([product_id]))
        }

    }

    function getCartProductId() {
        showHideModal();
        setCartId(props.product_id);
    }


    //func to save or delete a phone to/from localstorage when it was confirmed via modalwindow
    function saveToTheCart() {
        showHideModal();

        const {product_id} = props;

        const getItem = localStorage.getItem('bought');

        if (getItem) {
            const parsedItem = JSON.parse(getItem)

            const foundId = parsedItem.find(id => id === product_id)

            if (foundId) {
                alert('This product is already in your shopping cart, dude')
                return null;
            } else {
                parsedItem.push(product_id);
            }

            localStorage.setItem('bought', JSON.stringify(parsedItem));
        } else {
            localStorage.setItem('bought', JSON.stringify([product_id]))
        }

    }

    function deleteFromTheCart() {

        const {product_id} = props;

        const getItem = localStorage.getItem('bought');

        const parsedItem = JSON.parse(getItem)

        const foundIndex = parsedItem.findIndex(indexId => indexId === product_id)

        parsedItem.splice(foundIndex, 1);

        localStorage.setItem('bought', JSON.stringify(parsedItem));

    }

    return (
        <>

            {
                isModalOpen && <Modal title={modalTitle} body={modalBody} showHideModal={showHideModal} saveToTheCart={saveToTheCart}  />
            }

            <Phone
                {...props}
                getCartProductId={getCartProductId}
                deleteFromTheCart={deleteFromTheCart}
                favIcon={favoriteStarIcon}
                addCartIcon={addToCartIcon}
                delCartIcon={cancelCartIcon}
                isInHome={isInHome}
                isInCart={isInCart}
            />

        </>
    );

};

export default ProductList;