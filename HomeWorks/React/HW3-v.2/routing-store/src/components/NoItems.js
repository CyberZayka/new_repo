import React from 'react';

const NoItems = () => {
    return (
        <div className='no-items-style'>

            <h1>No items added here</h1>

        </div>
    );
};

export default NoItems;