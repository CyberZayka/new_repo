import React from "react";

const Price = (props) => {
    const {priceColor, price} = props;
    return <h4 style={{marginBottom:'20px'}}>Price:  <span style={{color: priceColor}}>{price}UAH</span></h4>
}

export default Price;