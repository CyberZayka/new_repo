import React from 'react'

const Button = (props) => {

    const {classBtn, textBtn, icon, action, id} = props;

    return (
        <button
            className={classBtn}
            onClick={action}
            id={id}
        >
            {textBtn}
            {icon}
        </button>
    )

}

export default Button;