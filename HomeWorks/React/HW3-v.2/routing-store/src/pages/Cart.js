import React, {useEffect, useState} from 'react'

import ProductList from "../components/ProductList";
import NoItems from "../components/NoItems";

const Cart = () => {

    const [allPhones, setAllPhones] = useState([]);
    const [allBoughtPhonesId, setAllBoughtPhonesId] = useState([]);
    const [priceColor] = useState('red');
    const [deleteFromCartBtnText] = useState('Delete from the cart');
    const [deleteFromCartBtnClass] = useState('delete-button');
    const [isEmptyCart, setIsEmptyCart] = useState(false);

    const modalTitle = "Deleting a product";
    const modalBody = "Are you sure you want to delete from the cart this product?";

    useEffect(() => {

        //get all data from a server
        const fetchData = async () => {
            const response = await fetch('/phones.json')
            const data = await response.json();
            const {phones} = data;
            setAllPhones(phones);
        }

        fetchData();
    },[])

    useEffect(() => {

        const boughtItemsArray = localStorage.getItem('bought')

        if (boughtItemsArray) {
            const parsedArray = JSON.parse(boughtItemsArray)
            setAllBoughtPhonesId(parsedArray)
            if (parsedArray.length === 0) {
                localStorage.removeItem('bought');
            }
        } else {
            setIsEmptyCart(true)
        }

    },[allBoughtPhonesId])

    const renderPhones = allBoughtPhonesId.map(phoneId => {
        let productList;

        allPhones.map(phone => {
            if (phone.product_id === phoneId) productList = <ProductList
                {...phone}
                priceColor={priceColor}
                cartBtnText={deleteFromCartBtnText}
                cartBtnClass={deleteFromCartBtnClass}
                isInCart
                modalTitle={modalTitle}
                modalBody={modalBody}
            />
            return allPhones;
        })

        return productList;
    })

    const renderProductList =
        <ul className="flex-wrap">
            {renderPhones}
        </ul>


    return (
        <>
            {isEmptyCart ? <NoItems /> : renderProductList}
        </>
    )

}

export default Cart;