import React,{useState, useEffect} from 'react'

import ProductList from "../components/ProductList";

const Home = () => {

    const [allPhones, setAllPhones] = useState([]);
    const [priceColor] = useState('red');
    const [addToCartBtnText] = useState('Add to the cart');
    const [addToCartBtnClass] = useState('add-button');
    const [isInHome] = useState(true);

    const modalTitle = "Saving a product";
    const modalBody = "Are you sure you want to add to the cart this product?";


    useEffect(() => {

        //get all data from a server
        const fetchData = async () => {
            const response = await fetch('/phones.json')
            const data = await response.json();
            const {phones} = data;
            setAllPhones(phones);
        }

        fetchData();
    },[])



    const renderPhones = allPhones.map(phone => {

        return <ProductList
            {...phone}
            priceColor={priceColor}
            cartBtnText={addToCartBtnText}
            cartBtnClass={addToCartBtnClass}
            modalTitle={modalTitle}
            modalBody={modalBody}
            isInHome={isInHome}
        />


    })


    return (
        <>
            <ul className="flex-wrap">
                {renderPhones}
            </ul>
        </>
    )

}

export default Home;