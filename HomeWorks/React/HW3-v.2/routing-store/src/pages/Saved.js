import React, {useEffect, useState} from 'react';

import ProductList from "../components/ProductList";
import NoItems from "../components/NoItems";


const Saved = () => {

    const [allPhones, setAllPhones] = useState([]);
    const [allSavedPhonesId, setAllSavedPhonesId] = useState([]);
    const [priceColor] = useState('red');
    const [addToCartBtnText] = useState('Add to the cart');
    const [addToCartBtnClass] = useState('add-button');
    const [isInHome] = useState(true);
    const [isEmptyFav, setIsEmptyFav] = useState(false);

    const modalTitle = "Saving a product";
    const modalBody = "Are you sure you want to add to the cart this product?";


    useEffect(() => {

        //get all data from a server
        const fetchData = async () => {
            const response = await fetch('/phones.json')
            const data = await response.json();
            const {phones} = data;
            setAllPhones(phones);
        }

        fetchData();
    },[])

    useEffect(() => {

        ///get data of favorite from localstorage
        const savedItemsArray = localStorage.getItem('favorite');

        //check if response from locastorage is not null and there is something
        if (savedItemsArray) {

            //if there is something in the localstorage, parse string to object
            const parsedArray = JSON.parse(savedItemsArray)
            setAllSavedPhonesId(parsedArray)
            if (parsedArray.length === 0) {
                localStorage.removeItem('favorite');
            }
        } else {
            setIsEmptyFav(true)
        }


    }, [allSavedPhonesId])


    const renderPhones = allSavedPhonesId.map(phoneId => {

        let productList;

        allPhones.map(phone => {
            if (phone.product_id === phoneId) productList = <ProductList
                {...phone}
                priceColor={priceColor}
                cartBtnText={addToCartBtnText}
                cartBtnClass={addToCartBtnClass}
                modalTitle={modalTitle}
                modalBody={modalBody}
                isInHome={isInHome}
            />
            return allPhones;
        })

        return productList;
    })

    const renderProductList =
        <ul className="flex-wrap">
            {renderPhones}
        </ul>

    return (
        <>
            {isEmptyFav ? <NoItems /> : renderProductList}
        </>
    )

}

export default Saved;