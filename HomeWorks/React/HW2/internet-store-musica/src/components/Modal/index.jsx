import React, {Component} from 'react';
import styled from "styled-components";

import Button from "../Button";

class Modal extends Component {
    render() {
        const {headerTitle, bodyTitle, bodyText, makeOrder, cancelOrder, bgColorDeleting, bgColorSaving, isOpenModal,id} = this.props

        return (
            <MainWrapperModal id={id} isOpenModal={isOpenModal}>
                <ModalDialog>
                    <ModalHead
                        bgColorDeleting={bgColorDeleting}
                        bgColorSaving={bgColorSaving}
                    >
                        <ModalHeaderTitle
                            bgColorDeleting={bgColorDeleting}
                            bgColorSaving={bgColorSaving}
                        >
                            {headerTitle}
                        </ModalHeaderTitle>
                    </ModalHead>
                    <ModalBody
                        bgColorDeleting={bgColorDeleting}
                        bgColorSaving={bgColorSaving}
                    >
                        <ModalBodyText>
                            <ModalBodyTitle
                                bgColorDeleting={bgColorDeleting}
                                bgColorSaving={bgColorSaving}
                            >
                                {bodyTitle}
                                <br/>
                                {bodyText}
                            </ModalBodyTitle>
                        </ModalBodyText>
                        <ModalButtonWrap>
                            <Button
                                primaryModal
                                bgColorSaving={bgColorSaving}
                                bgColorDeleting={bgColorDeleting}
                                text="Ok"
                                onClick={makeOrder}
                            />
                            <Button
                                primaryModal
                                bgColorSaving={bgColorSaving}
                                bgColorDeleting={bgColorDeleting}
                                text="Cancel"
                                onClick={cancelOrder}
                            />
                        </ModalButtonWrap>
                    </ModalBody>
                </ModalDialog>
            </MainWrapperModal>
        );
    }
}


const MainWrapperModal = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1;
  display: block;
  width: 100%;
  height: 100%;
  overflow: auto;
  outline: 0;
  
  background-color: ${props => props.isOpenModal ? "rgba(0,0,0,0.4)" : "none"};
  
`

const ModalDialog = styled.div`
  max-width: 516px;
  margin: 1.75rem auto;
`

const ModalHeaderTitle = styled.h3`
  font-family: HelveticaNeue-Medium, AdobeInvisFont, MyriadPro-Regular;
  font-size: 16pt;
  color: ${props => props.bgColorDeleting && "rgba(255, 255, 255, 255)"};
  
  color: ${props => props.bgColorSaving && "black"};
`

const ModalBodyTitle = styled.h5`
  font-family: HelveticaNeue, AdobeInvisFont, MyriadPro-Regular;
  font-size: 10pt;
  color: ${props => props.bgColorDeleting && "rgba(255, 255, 255, 255)"};

  color: ${props => props.bgColorSaving && "black"};
`

const ModalHead = styled.div`
  width: 100%;
  height: 68px;
  display: flex;
  justify-content: flex-start;
  border-radius: 8px 8px 0 0;
  background-color: ${props => props.bgColorDeleting && "#d44637"};
  
  background-color: ${props => props.bgColorSaving && "#a2ccd3"};
`

const ModalBody = styled.div`
  width: 100%;
  height: 184px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border-radius: 0 0 8px 8px;
  background-color: ${props => props.bgColorDeleting && "#e74c3c"};

  background-color: ${props => props.bgColorSaving && "#a2ccd3"};
`

const ModalBodyText = styled.div`
  width: 100%;
  padding: 15px;
  text-align: center;
`

const ModalButtonWrap = styled.div`
  width: 100%;
  padding: 7px;
  margin-bottom: 25px;
  display: flex;
  justify-content: center;
`

export default Modal;