import React from 'react';

const Footer = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
            <span className="navbar-brand">&copy; All rights reserved by Lekhich</span>
        </nav>
    )
}

export default Footer;