import React,{useState, useEffect} from 'react';

import Icon from "./Icon";
import {phoneIdActions, phoneIdSelectors} from "../redux/phoneIdStore";
import {modalWindowActions, modalWindowSelectors} from "../redux/modalWindowStore";
import {useDispatch, useSelector} from "react-redux";
import Modal from "./Modal";
import PhoneLi from "./PhoneLi";

const Phone = (props) => {
    const {
        product_id,
        cartBtnClass,
        cartBtnText,
        isInHome,
        isInCart,
        modalTitle,
        modalBody,
        phone,
        keyId
    } = props;


    const dispatch = useDispatch();
    const isOpenModal = useSelector(modalWindowSelectors.isOpenModal)
    const cartId = useSelector(phoneIdSelectors.cartId)

    const [allSavedPhonesId, setAllSavedPhonesId] = useState([]);
    const [starColor, setStarColor] = useState('lightgrey');

    const favoriteStarIcon = <Icon type="star" classes="star-style" color={starColor} onClick={saveFavoriteProductToStorage} />
    const addToCartIcon = <Icon type="cart" classes="cart-style" color="white" />
    const cancelCartIcon = <Icon type='cancel' classes='cancel-style' />


    //get data from localstorage 'favorite' in order to compare the current id of the star with the one saved in the storage
    useEffect(() => {

        const savedItemsArray = localStorage.getItem('favorite');

        if (savedItemsArray) {
            const parsedArray = JSON.parse(savedItemsArray);
            setAllSavedPhonesId(parsedArray)
        }

    }, [])


    //comparing id`s. if there is id in localstorage is the same with active id, then shine the star
    useEffect(() => {
        allSavedPhonesId.map(id => {
            if (id === product_id) setStarColor('gold')
            return allSavedPhonesId;
        })

    },[allSavedPhonesId, product_id])


    //show and hide modal window func with async
    const showHideModal = () => {
        dispatch(modalWindowActions(isOpenModal))
    }


    //func to save or delete a phone to/from localstorage, when a star was clicked, and shine this star
    function saveFavoriteProductToStorage() {

        const getItem = localStorage.getItem('favorite');

        if (getItem) {
            const parsedItem = JSON.parse(getItem)

            const foundId = parsedItem.find(id => id === product_id)

            if (foundId) {
                setStarColor('lightgrey');
                const foundIndex = parsedItem.findIndex(indexId => {
                    return indexId === product_id
                })
                parsedItem.splice(foundIndex, 1);
            } else {
                setStarColor('gold');
                parsedItem.push(product_id);
            }

            localStorage.setItem('favorite', JSON.stringify(parsedItem))
        } else {
            setStarColor('gold')
            localStorage.setItem('favorite', JSON.stringify([product_id]))
        }

    }

    //func to get id of product via click on button cart with async
    function getCartProductId() {
        showHideModal();
        dispatch(phoneIdActions.setCartId(product_id))
    }

    //func to save or delete a phone to/from localstorage when it was confirmed via modalwindow
    function saveToTheCart() {
        showHideModal();

        const getItem = localStorage.getItem('bought');

        if (getItem) {
            const parsedItem = JSON.parse(getItem)

            const foundId = parsedItem.find(id => id === cartId)

            if (foundId) {
                alert('This product is already in your shopping cart, dude')
                return null;
            } else {
                parsedItem.push(cartId);
            }

            localStorage.setItem('bought', JSON.stringify(parsedItem));
        } else {
            localStorage.setItem('bought', JSON.stringify([cartId]))
        }

    }

    function deleteFromTheCart() {
        showHideModal();

        const getItem = localStorage.getItem('bought');

        const parsedItem = JSON.parse(getItem)

        const foundIndex = parsedItem.findIndex(indexId => indexId === cartId)

        parsedItem.splice(foundIndex, 1);

        localStorage.setItem('bought', JSON.stringify(parsedItem));

    }



    return (
        <>

                {
                    isOpenModal
                    &&
                    <Modal
                        title={modalTitle}
                        body={modalBody}
                        showHideModal={showHideModal}
                        saveOrDeleteCart={
                            isInHome ? saveToTheCart : deleteFromTheCart
                        }
                    />
                }

                <PhoneLi
                    {...phone}
                    favoriteStarIcon={favoriteStarIcon}
                    cancelCartIcon={cancelCartIcon}
                    addToCartIcon={addToCartIcon}
                    getCartProductId={getCartProductId}
                    key={keyId}
                    isInHome={isInHome}
                    isInCart={isInCart}
                    cartBtnClass={cartBtnClass}
                    cartBtnText={cartBtnText}
                />
        </>
    );
};

export default Phone;