import React from 'react';
import * as AllIcons from '../theme/icons/index';

const Icon = ({type, color, classes, ...restProps}) => {
    const currentIcon = AllIcons[type];

    if (!currentIcon) {
        return null
    }

    return (
        <div className={classes} {...restProps}>
            {currentIcon(color, classes)}
        </div>
    );
};

export default Icon;