import React,{useState} from "react";

const Price = (props) => {
    const [priceColor] = useState("red");

    return <h4 style={{marginBottom:'20px'}}>Price:  <span style={{color: priceColor}}>{props.price}UAH</span></h4>
}

export default Price;