import React from 'react';

import Phone from "./Phone";

const ProductList = (props) => {

    const {modalTitle, modalBody, isInHome, isInCart, phones} = props;

    return (
        <ul className="flex-wrap">
            {
                phones
                &&
                phones.map(phone => {
                    return <Phone
                        {...props}
                        product_id={phone.product_id}
                        key={phone.product_id}
                        keyId={phone.product_id}
                        phone={phone}
                        isInHome={isInHome}
                        isInCart={isInCart}
                        modalTitle={modalTitle}
                        modalBody={modalBody}
                    />
                })
            }
        </ul>
    )


};

export default ProductList;