import types from "./types";

const initialStore = {
    arrayOfAllPhones: [],
    arrayOfSavedPhones: []
}


const phoneReducer = (currentStore = initialStore, action) => {
    switch (action.type) {
        case types.SAVE__ALL_PHONES:
            return {
                arrayOfAllPhones: action.payload
            }

        case types.SAVED_PHONES_IN_STORAGE:
            return {
                ...currentStore,
                arrayOfSavedPhones: action.payload
            }

        default:
            return currentStore;
    }
}

export default phoneReducer;