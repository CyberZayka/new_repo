import types from "./types";


const saveAllPhonesAction = allPhones => ({
    type: types.SAVE__ALL_PHONES,
    payload: allPhones
})

const savedPhonesAction = savedPhones => ({
    type: types.SAVED_PHONES_IN_STORAGE,
    payload: savedPhones
})


const asyncSaveAllPhones = () => {
    return dispatch => {
        fetch('/phones.json')
            .then(response => response.json())
            .then(data => {
                dispatch(saveAllPhonesAction(data))
            })
    }
}

/*
const asyncSavedPhonesAction = () => {

    return dispatch => {
        fetch('/phones.json')
            .then(response => response.json())
            .then(data => {
                const {phones} = data;
                const arrayToDispatch = [];
                phones.map(phone => {
                    const savedItems = JSON.parse(localStorage.getItem('favorite'));

                    if (savedItems) {
                        savedItems.map(id => {
                            if (phone.product_id === id) arrayToDispatch.push(phone)
                        })
                    }
                })
                dispatch(savedPhonesAction(arrayToDispatch))
            })
    }

}*/


const asyncSaveSavedBoughtPhones = (folder) => {

    return dispatch => {
        fetch('/phones.json')
            .then(response => response.json())
            .then(data => {
                const {phones} = data;
                const arrayToDispatch = [];
                phones.map(phone => {
                    const savedItems = JSON.parse(localStorage.getItem(folder));

                    if (savedItems) {
                        savedItems.map(id => {
                            if (phone.product_id === id) arrayToDispatch.push(phone)
                        })
                    }
                })
                dispatch(savedPhonesAction(arrayToDispatch))
            })
    }

}


export default {
    asyncSaveAllPhones: asyncSaveAllPhones,
    asyncSavedPhones: asyncSaveSavedBoughtPhones
}