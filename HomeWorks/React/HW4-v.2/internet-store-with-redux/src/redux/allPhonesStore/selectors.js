
const selectAllPhones = currentStore => currentStore.allPhones.arrayOfAllPhones;
const selectSavedPhones = currentStore => currentStore.allPhones.arrayOfSavedPhones;

export default {
    allPhonesSelector: selectAllPhones,
    savedPhones: selectSavedPhones
}