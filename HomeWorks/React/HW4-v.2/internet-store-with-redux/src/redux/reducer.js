import {combineReducers} from "redux";
import allPhones from "./allPhonesStore/index"
import modalWindow from "./modalWindowStore/index"
import phoneId from "./phoneIdStore/index"

export default combineReducers({
    allPhones,
    modalWindow,
    phoneId
})