import IS_OPEN_MODAL from "./types";

const initialStore = {
    isOpenModal: false,
}


const modalWindowReducer = (currentStore = initialStore, action) => {
    switch (action.type) {
        case IS_OPEN_MODAL:
            return {
                ...currentStore,
                isOpenModal: action.payload
            }

        default:
            return currentStore;
    }
}

export default modalWindowReducer;