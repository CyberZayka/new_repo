import reducer from "./reducers";


export {default as modalWindowSelectors} from "./selectors";
export {default as modalWindowActions} from "./actions";
export {default as modalWindowTypes} from "./types";

export default reducer