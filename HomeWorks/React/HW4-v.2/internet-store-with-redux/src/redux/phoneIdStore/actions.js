import types from "./types";

const cartIdAction = cartId => ({
    type: types.CART_ID,
    payload: cartId
})


export default {
    setCartId: cartIdAction,
};