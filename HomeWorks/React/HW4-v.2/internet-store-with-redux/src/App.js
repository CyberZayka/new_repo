import React from 'react'

import Navbar from "./components/Navbar";
import AppRoutes from "./pages/route/AppRoutes";
import Footer from "./components/Footer";

const App = () => {

    return (
        <div className="App">
          <Navbar />
          <AppRoutes />
          <Footer />
        </div>
    );
}

export default App;