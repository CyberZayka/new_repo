import React from "react";

const Color = (props) => {
    const {color} = props;

    return (
        <div className="flex">
            <h4>{`Color: ` + color}</h4>
            <div className="square" style={{backgroundColor: color}}> </div>
        </div>
    )
}

export default Color;