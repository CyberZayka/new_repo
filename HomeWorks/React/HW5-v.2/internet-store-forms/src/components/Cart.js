import React, {useEffect, useState} from 'react';
import {useDispatch,useSelector} from 'react-redux';

import ProductList from "../components/ProductList";
import NoItems from "../components/NoItems";
import BuyAllComponent from "../components/BuyAllComponent";
import FormToOrder from "../components/Form/Form";
import {phonesActions, phonesSelectors} from "../redux/allPhonesStore";

const Cart = () => {
    const dispatch = useDispatch();
    const boughtPhones = useSelector(phonesSelectors.savedPhones)

    const [data, setData] = useState();

    const [deleteFromCartBtnText] = useState('Delete from the cart');
    const [deleteFromCartBtnClass] = useState('delete-button');
    const [isEmptyCart, setIsEmptyCart] = useState(false);
    const [isBoughtPhone, setIsBoughtPhone] = useState(false);

    const modalTitle = "Deleting a product";
    const modalBody = "Are you sure you want to delete from the cart this product?";

    const folder = 'bought';

    const handleClickBuyAll = () => {
        setIsBoughtPhone(!isBoughtPhone)
    }

    const renderAllBoughtPhones = () => {
        return <BuyAllComponent handleClickBuyAll={handleClickBuyAll} />
    }

    useEffect(() => {
        dispatch(phonesActions.asyncSavedPhones(folder))
    }, [dispatch])


    useEffect(() => {

        const boughtItemsArray = localStorage.getItem('bought')

        if (boughtItemsArray) {
            const parsedArray = JSON.parse(boughtItemsArray)
            setData(parsedArray)
            if (parsedArray.length === 0) {
                localStorage.removeItem('bought');
            }
        } else {
            setIsEmptyCart(true)
        }

    },[data])



    const renderProductList =
        <ProductList
            renderAllBoughtPhones={renderAllBoughtPhones}
            phones={boughtPhones}
            cartBtnText={deleteFromCartBtnText}
            cartBtnClass={deleteFromCartBtnClass}
            modalTitle={modalTitle}
            modalBody={modalBody}
            isInCart
        />


    return (
        <>
            {isEmptyCart ? <NoItems /> : renderProductList}
            {isBoughtPhone && <FormToOrder />}
        </>
    )

}


export default Cart;