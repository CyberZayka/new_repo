import React from "react";
import {Formik, Form, Field} from "formik";

import CustomInput from "./CustomInput";
import orderFormSchema from "./validationSchema";

const FormToOrder = () => {

    const handleSubmit = (values) => {
        console.log(values);
    }

    return (
        <Formik
            initialValues={{
                firstName: '',
                lastName: '',
                age: '',
                deliveryAddress: '',
                mobilePhone: ''
            }}
            onSubmit={handleSubmit}
            validationSchema={orderFormSchema}
        >
            {(formikProps) => {
                return (
                    <Form className='order-form' noValidate>
                        <h4>Order</h4>

                        <Field component={CustomInput} label='First name' type='text' placeholder='First name' name='firstName' />
                        <Field component={CustomInput} label='Last name' type='text' placeholder='Last name' name='lastName' />
                        <Field component={CustomInput} label='Age' type='text' placeholder='Age' name='age' />
                        <Field component={CustomInput} label='Delivery Address' type='text' placeholder='Delivery address' name='deliveryAddress' />
                        <Field component={CustomInput} label='Mobile Phone' type='text' placeholder='Mobile phone' name='mobilePhone' />

                        <div>
                            <button type='submit'>Checkout</button>
                        </div>
                    </Form>
                )
            }}
        </Formik>
    )

}

export default FormToOrder;