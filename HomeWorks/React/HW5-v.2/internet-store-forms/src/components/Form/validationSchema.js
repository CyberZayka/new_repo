import * as yup from 'yup';

const REQUIRED_FIELD = 'this field is required';

const orderFormSchema = yup.object().shape({
    firstName: yup
        .string()
        .required(REQUIRED_FIELD),
    lastName: yup
        .string()
        .required(REQUIRED_FIELD),
    age: yup
        .number()
        .required(REQUIRED_FIELD),
    deliveryAddress: yup
        .string()
        .required(REQUIRED_FIELD),
    mobilePhone: yup
        .string()
        .required(REQUIRED_FIELD)
});

export default orderFormSchema;