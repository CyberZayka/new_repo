import React from 'react'
import Color from "./Color";
import Price from "./Price";
import Button from "./Button";

const PhoneLi = (props) => {
    const {
        model,
        product_id,
        color,
        price,
        avatar,
        cartBtnClass,
        cartBtnText,
        isInHome,
        isInCart,
        favoriteStarIcon,
        cancelCartIcon,
        addToCartIcon,
        getCartProductId
    } = props;


    return (
        <li className="li flex card-box flex-card">
            <div className="flex-column">
                <div className="flex-elem">
                    <h2>
                        {model}
                    </h2>
                    {isInHome && favoriteStarIcon}
                </div>
                <h5>
                    {`Product id: ${product_id}`}
                </h5>
                <Color
                    color={color}
                />
                <Price
                    price={price}
                />
                <img src={avatar} alt='phone' />
                <div className="flex-center">
                    <Button
                        classBtn={cartBtnClass}
                        textBtn={cartBtnText}
                        icon={
                            isInCart ? cancelCartIcon : addToCartIcon
                        }
                        action={getCartProductId}
                    />
                </div>
            </div>
        </li>
    )
}

export default PhoneLi;