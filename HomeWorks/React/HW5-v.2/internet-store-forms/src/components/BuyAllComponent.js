import React from "react";

import Button from "./Button";
import Icon from "./Icon";


const BuyAllComponent = (props) => {
    const buyIcon = <Icon type="cart" classes="cart-style" color="white" />

    return (
        <nav className="navbar navbar-light bg-light navbar-mine">
            <Button classBtn="btn btn-danger order-button" textBtn="Buy " icon={buyIcon} action={props.handleClickBuyAll} />
        </nav>
    )
}

export default BuyAllComponent;