import reducer from "./reducers";


export {default as phonesSelectors} from "./selectors";
export {default as phonesActions} from "./actions";
export {default as phonesTypes} from "./types";

export default reducer