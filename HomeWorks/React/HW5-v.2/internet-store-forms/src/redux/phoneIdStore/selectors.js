
const selectCartId = currentStore => currentStore.phoneId.cartId;

export default {
    cartId: selectCartId,
}