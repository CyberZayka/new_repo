import types from "./types"

const initialStore = {
    cartId: [],
}


const phoneIdReducer = (currentStore = initialStore, action) => {
    switch (action.type) {

        case types.CART_ID:
            return {
                ...currentStore,
                cartId: action.payload
            }

        default:
            return currentStore;
    }
}

export default phoneIdReducer;