import reducer from "./reducers";

export {default as phoneIdSelectors} from "./selectors";
export {default as phoneIdActions} from "./actions";
export {default as phoneIdTypes} from "./types";

export default reducer