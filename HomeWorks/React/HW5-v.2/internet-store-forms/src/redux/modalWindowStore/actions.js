import IS_OPEN_MODAL from "./types";

const isOpenModalAction = isOpenModal => ({
    type: IS_OPEN_MODAL,
    payload: !isOpenModal
})


export default isOpenModalAction;