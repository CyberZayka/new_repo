
const selectIsOpenModal = currentStore => currentStore.modalWindow.isOpenModal;

export default {
    isOpenModal: selectIsOpenModal
}