import React,{useState, useEffect} from 'react'
import {useSelector, useDispatch} from 'react-redux';

import ProductList from "../components/ProductList";
import {phonesSelectors, phonesActions} from "../redux/allPhonesStore";

const Home = () => {
    const dispatch = useDispatch();
    const allPhones = useSelector(phonesSelectors.allPhonesSelector)

    const {phones} = allPhones;

    const [addToCartBtnText] = useState('Add to the cart');
    const [addToCartBtnClass] = useState('add-button');

    const modalTitle = "Saving a product";
    const modalBody = "Are you sure you want to add to the cart this product?";

    //hook to save all allPhonesStore in redux store with async
    useEffect(() => {
        dispatch(phonesActions.asyncSaveAllPhones())
    }, [dispatch])


    return (
        <ProductList
            phones={phones}
            cartBtnText={addToCartBtnText}
            cartBtnClass={addToCartBtnClass}
            modalTitle={modalTitle}
            modalBody={modalBody}
            isInHome
        />
    )
}


export default Home;