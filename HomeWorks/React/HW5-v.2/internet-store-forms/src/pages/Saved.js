import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';

import ProductList from "../components/ProductList";
import NoItems from "../components/NoItems";
import {phonesActions, phonesSelectors} from "../redux/allPhonesStore";

const Saved = () => {
    const dispatch = useDispatch();
    const savedPhones = useSelector(phonesSelectors.savedPhones)

    const [data, setData] = useState();

    const [addToCartBtnText] = useState('Add to the cart');
    const [addToCartBtnClass] = useState('add-button');
    const [isEmptyFav, setIsEmptyFav] = useState(false);

    const modalTitle = "Saving a product";
    const modalBody = "Are you sure you want to add to the cart this product?";

    const folder = 'favorite';

    useEffect(() => {
        dispatch(phonesActions.asyncSavedPhones(folder))
    },[dispatch])


    useEffect(() => {
        const savedItemsArray = localStorage.getItem('favorite')

        if (savedItemsArray) {
            const parsedArray = JSON.parse(savedItemsArray)
            setData(parsedArray)
            if (parsedArray.length === 0) {
                localStorage.removeItem('favorite');
            }
        } else {
            setIsEmptyFav(true)
        }
    },[data])


    const renderProductList =
        <ProductList
            phones={savedPhones}
            cartBtnText={addToCartBtnText}
            cartBtnClass={addToCartBtnClass}
            modalTitle={modalTitle}
            modalBody={modalBody}
            isInHome
        />

    return (
        <>
            {isEmptyFav ? <NoItems /> : renderProductList}
        </>
    )

}


export default Saved;