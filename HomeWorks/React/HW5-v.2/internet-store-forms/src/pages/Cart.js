import React, {useEffect, useState} from 'react';
import {useDispatch,useSelector} from 'react-redux';

import ProductList from "../components/ProductList";
import NoItems from "../components/NoItems";
import {phonesActions, phonesSelectors} from "../redux/allPhonesStore";

const Cart = () => {
    const dispatch = useDispatch();
    const boughtPhones = useSelector(phonesSelectors.savedPhones)

    const [data, setData] = useState();

    const [deleteFromCartBtnText] = useState('Delete from the cart');
    const [deleteFromCartBtnClass] = useState('delete-button');
    const [isEmptyCart, setIsEmptyCart] = useState(false);

    const modalTitle = "Deleting a product";
    const modalBody = "Are you sure you want to delete from the cart this product?";

    const folder = 'bought';

    useEffect(() => {
        dispatch(phonesActions.asyncSavedPhones(folder))
    }, [dispatch])


    useEffect(() => {

        const boughtItemsArray = localStorage.getItem('bought')

        if (boughtItemsArray) {
            const parsedArray = JSON.parse(boughtItemsArray)
            setData(parsedArray)
            if (parsedArray.length === 0) {
                localStorage.removeItem('bought');
            }
        } else {
            setIsEmptyCart(true)
        }

    },[data])



    const renderProductList =
        <ProductList
            phones={boughtPhones}
            cartBtnText={deleteFromCartBtnText}
            cartBtnClass={deleteFromCartBtnClass}
            modalTitle={modalTitle}
            modalBody={modalBody}
            isInCart
        />


    return (
        <>
            {isEmptyCart ? <NoItems /> : renderProductList}
        </>
    )

}


export default Cart;