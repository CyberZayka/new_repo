import {combineReducers} from "redux";

import allPhones from "./allPhones/index"
import modalWindow from "./modalWindow/index"

export default combineReducers({
    allPhones,
    modalWindow
})