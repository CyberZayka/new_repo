const selectAllPhones = currentStore => currentStore.allPhones.collectionOfAllPhones;
const selectFavsPhones = currentStore => currentStore.allPhones.collectionOfFavsPhones;

export default {
    allPhonesSelector: selectAllPhones,
    favsPhonesSelector: selectFavsPhones
}