import types from "./types"

const saveAllPhonesAction = allPhones => ({
    type: types.SAVE_ALL_PHONES,
    payload: allPhones
})

const saveFavsPhonesAction = favsPhones => ({
    type: types.SAVE_FAVS_PHONES,
    payload: favsPhones
})

const saveBoughtPhonesAction = boughtPhones => ({
    type: types.SAVE_BOUGHT_PHONES,
    payload: boughtPhones
})

const asyncSaveAllPhones = () => {
    return dispatch => {
        fetch('phones.json')
        .then(response => response.json())
        .then(data => {
            dispatch(saveAllPhonesAction(data))
        })
    }
}

 
export default {
    asyncSaveAllPhones,
    saveFavsPhonesAction,
    saveBoughtPhonesAction,
}