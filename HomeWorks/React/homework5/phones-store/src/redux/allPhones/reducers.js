import types from "./types"

const initialStore = {
    collectionOfAllPhones: [],
    collectionOfFavoritePhones: [],
    coolectionOfBoughtPhones: []
}

const phoneReducer = (currentStore = initialStore, action) => {
    switch(action.type) {
        case types.SAVE_ALL_PHONES:
            return {
                ...currentStore,
                collectionOfAllPhones: action.payload,
            }
        case types.SAVE_FAVS_PHONES:
            return {
                ...currentStore,
                collectionOfFavoritePhones: action.payload
            }    
        case types.SAVE_BOUGHT_PHONES:
            return {
                ...currentStore,
                collectionOfBoughtPhones: action.payload
            }    

        default:
            return currentStore    
    }
}

export default phoneReducer