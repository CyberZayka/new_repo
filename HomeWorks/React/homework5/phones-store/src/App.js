import React from "react";
import Header from './components/Header/Header'
// import Slider from './components/Slider/Slider'
import Footer from "./components/Footer/Footer";
import AppRoutes from "./components/Routes/AppRoutes"

const App = () => {
    return (
        <div className="App">
            <Header/>
            {/* <Slider /> */}
            <AppRoutes />
            <Footer />
        </div>
    );
}

export default App;
