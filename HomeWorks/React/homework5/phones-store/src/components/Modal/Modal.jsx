import React, {memo} from 'react';
import './Modals.scss'
import PropTypes from 'prop-types'

const Modal = (props) => {
   const {actions, toggleModal, isCart} = props;
   
   return (
      <>
         <div className="modal-wrapper">
            <div className="modal-bg-wrapper"
                  onClick={toggleModal}/>
               <div className="modal-inner">
                  <div className="modal login">
                     <p className="modal__header-text">
                        {isCart ? "Remove from the cart?" : "Add to Cart?"}
                     </p>
                     <p className="modal__main-text">
                        {actions}
                     </p>
                  </div>
               </div>
            </div>
      </>
   );
}

export default memo(Modal);

Modal.propTypes = {
   btn: PropTypes.object
};
