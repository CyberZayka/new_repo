import React, {useState, memo, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import Phone from "./Phone";
import PropTypes from 'prop-types'
import Modal from "../Modal/Modal";
import Button from "../Button/Button"
import FormCart from "../FormCart/FormCart"
import {modalWindowSelectors, modalWindowActions} from "../../redux/modalWindow"

const PhonesList = (props) => {


   const dispatch = useDispatch()
   const isOpenModal = useSelector(modalWindowSelectors.isOpenModal)

   const [currentId, setCurrentId] = useState(null)
   const [cart, setCart] = useState([])
   const [showForm, setShowForm] = useState(false)

   const {collection, removeFromFavorites, isCart, removeFromCart} = props;

   useEffect(() => {
      let cleanUpFunction = false
      if(cart.length > 0) localStorage.setItem('cart', JSON.stringify(cart))

      return () => cleanUpFunction = true
   }, [cart])

   const addToCart = () => {
      
      const cartStorage = JSON.parse(localStorage.getItem("cart"))

      if(cartStorage) {
         const index = cartStorage.indexOf(currentId)
         if(index === -1) {
            setCart([...cartStorage, currentId])
         } else {
            return null
         }
      } else {
         setCart([currentId])
      }
      
   }

   const handleShowForm = () => {
      setShowForm(!showForm)
   }


   const addToFavorites = (product_id) => {
     
      const isFav = localStorage.getItem("isFav")

        if(isFav) {
            const isFavoriteArray = JSON.parse(isFav)

            const index = isFavoriteArray.indexOf(product_id)
            if(index === -1) {
                localStorage.setItem("isFav", JSON.stringify([...isFavoriteArray, product_id]))
            } else {
                isFavoriteArray.splice(index, 1)
                isFavoriteArray.length === 0 ? localStorage.removeItem("isFav") : localStorage.setItem("isFav", JSON.stringify(isFavoriteArray))
            }

        } else {
            localStorage.setItem("isFav", JSON.stringify([product_id]))
        }
   }


   const setCurrentIdFunc = (id) => {
      setCurrentId(id)
   }

   const modalOpen = () => {
      dispatch(modalWindowActions(isOpenModal))
   }


   const cardsList = collection.map(card => {

      return (
         <Phone
            key={card.product_id}
            className="card"
            imgUrl={card.avatar}
            name={card.model}
            price={card.price}
            color={card.color}
            id={card.product_id}
            addToCart={() => {
               setCurrentIdFunc(card.product_id);
               modalOpen();
            }}
            addToFavorites={() => {
               props.isFavorite ? removeFromFavorites(card.product_id) : addToFavorites(card.product_id)
            }}
            isCart={props.isCart}
         />
      )
   });

   return (
         <>
            {
               isCart && <Button text="Order" onClickHandler={handleShowForm} />
            }
            {
               collection.length > 0 && <div className="cards-wrapper container">{cardsList}</div>
            }
            {
               showForm && <FormCart toggleModal={handleShowForm} removeFromCart={removeFromCart} />
            }
            {isOpenModal && (
               <>
                  <Modal
                     isCart={props.isCart}
                     toggleModal={() => modalOpen()}
                     actions={
                        <>
                           <button onClick={() => {
                              isCart ? removeFromCart(currentId) : addToCart();
                              modalOpen();
                           }}
                                 className="close__modal-wrapper">
                              Yes
                           </button>

                           <button
                                 onClick={() => modalOpen()}
                                 className="close__modal-wrapper">
                              No
                           </button>
                        </>
                     }
                  />
               </>
            )}
         </>
   );
}

export default memo(PhonesList);

PhonesList.propTypes = {
   collection: PropTypes.array,
};
