import React, {useState, useEffect, memo} from 'react';
import PropTypes from 'prop-types';
import Button from "../Button/Button";
import Icon from "../Icon/Icon"
import './styles.scss'

const Phone = (props) => {

   const [starColor, setStarColor] = useState("lightgrey")

   useEffect(() => {
      const {id} = props

      const isFav = localStorage.getItem("isFav")

      if(isFav) {
         const isFavoriteArray = JSON.parse(isFav)
         isFavoriteArray.forEach(fav => {
            if(+fav === id) setStarColor("gold")
         })
      }
   }, [props])

   const drawStar = () => {
      const {addToFavorites} = props
      starColor === "lightgrey" ? setStarColor("gold") : setStarColor("lightgrey")
      addToFavorites()
   }

   const cartIcon = <Icon type="cart" color="white" classes="svg-inline--fa fa-shopping-cart fa-w-18 cart-size" />
   const {imgUrl, name, price, color, addToCart, removeFromCart} = props;

   return (
      <div className="singleCard">
         <img src={imgUrl} alt="img"/>
             
         <div className="card-name-box">
            <span className="card-name">{name}</span>
            {props.isCart ? null : <Icon type="star" classes="star-size" color={starColor} onClick={drawStar} />}
         </div>

         <div className="price-box">
            <span className="card-price">Price: <span>{price} UAH</span></span>
            <span className="card-price">Color: {color} <div style={{backgroundColor: color}} className="color-box" ></div></span>
         </div>

         {props.isCart ? <Button text="Remove from the cart" onClickHandler={addToCart}/> : <Button text="Add to cart" icon={cartIcon} onClickHandler={addToCart}/>}
      </div>
   );
}

export default memo(Phone);

Phone.propTypes = {
   imgUrl: PropTypes.string,
   name: PropTypes.string,
   price: PropTypes.number,
   color: PropTypes.string,
};