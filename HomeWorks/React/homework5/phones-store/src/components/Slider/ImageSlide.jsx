import React, { PureComponent, memo } from "react";
import "./styles.scss";

class ImageSlide extends PureComponent {
  
  render() {

    return (
      <img src={this.props.srcUrl} alt="img" className="image-to-show"></img>
    );
  }
}

export default memo(ImageSlide);