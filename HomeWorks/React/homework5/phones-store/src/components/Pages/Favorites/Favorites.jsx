import React, {memo, useEffect, useState} from "react";
import {useDispatch} from "react-redux"
import { phonesActions } from "../../../redux/allPhones";

import PhonesList from "../../PhonesSection/PhonesList"

const Favorites = () => {
   const isFav = JSON.parse(localStorage.getItem("isFav"))

   const [listOfFavoritesPhones, setListOfFavoritesPhones] = useState([])

   const dispatch = useDispatch()

   useEffect(() => {
      getFavoritePhones()
   }, [])

   function getFavoritePhones() {

      fetch("phones.json")
      .then(response => response.json())
      .then(data => {
         if(isFav) {
            setListOfFavoritesPhones(
               isFav.map(phoneId => data.find(phone => phone.product_id === phoneId))
            )
         } else {
            setListOfFavoritesPhones(false)
         }
      })

   }


   const removeFromFavorites = product_id => {
      
      const index = isFav.indexOf(product_id)
      isFav.splice(index, 1)
      setListOfFavoritesPhones(
         listOfFavoritesPhones.filter(phone => phone.product_id !== product_id)
      )

      if(isFav.length === 0) {
         localStorage.removeItem("isFav")
         setListOfFavoritesPhones(false)
      } else {
         localStorage.setItem("isFav", JSON.stringify(isFav))
      }

   }

   dispatch(phonesActions.saveFavsPhonesAction(listOfFavoritesPhones))

   return (
         <>
            {
               listOfFavoritesPhones ? <PhonesList collection={listOfFavoritesPhones} isFavorite removeFromFavorites={removeFromFavorites} /> : <h1>No items added here</h1>
            }
         </>
   );
}

export default memo(Favorites);