import React, {useState, useEffect, memo} from "react";
import {useDispatch} from "react-redux"
import {phonesActions } from "../../../redux/allPhones";

import PhonesList from "../../PhonesSection/PhonesList"

const Cart = () => {

   const cart = JSON.parse(localStorage.getItem("cart"))

   const dispatch = useDispatch()
   
   const [listOfBoughtPhones, setListOfBoughtPhones] = useState([])

   const getBoughtPhones = async () => {
      const response = await fetch("phones.json")
      const data = await response.json()

      if(cart) {
         setListOfBoughtPhones(
            cart.map(phoneId => data.find(phone => phone.product_id === phoneId))
         )
      } else {
         setListOfBoughtPhones(false)
      }
   }

   useEffect(() => {
      getBoughtPhones()
   }, [])


   const removeFromCart = product_id => {
      if(product_id) {
         const index = cart.indexOf(product_id)
         cart.splice(index,1)

         setListOfBoughtPhones(
            listOfBoughtPhones.filter(phone => phone.product_id !== product_id)
         )

         if(cart.length === 0) {
            localStorage.removeItem("cart")
            setListOfBoughtPhones(false)
         } else {
            localStorage.setItem("cart", JSON.stringify(cart))
         }
      } else {
         localStorage.removeItem("cart")
         setListOfBoughtPhones(false)
      }
   }

   dispatch(phonesActions.saveBoughtPhonesAction(listOfBoughtPhones))

   return (
         <>
            {
               listOfBoughtPhones ? <PhonesList collection={listOfBoughtPhones} isCart removeFromCart={removeFromCart} /> : <h1>No items added here</h1>
            }
         </>
   );
}

export default memo(Cart);