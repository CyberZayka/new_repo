import React, {useEffect, memo} from "react";
import {useSelector, useDispatch} from 'react-redux';

import "./styles.scss";
import PhonesList from "../../PhonesSection/PhonesList";
import {phonesSelectors, phonesActions} from "../../../redux/allPhones"

const Home = () => {

   const dispatch = useDispatch()
   const listOfPhones = useSelector(phonesSelectors.allPhonesSelector)

   useEffect(() => {
      dispatch(phonesActions.asyncSaveAllPhones())
   }, [dispatch])

   
   return (
         <>
            <PhonesList collection={listOfPhones}/>
         </>
   );
}

export default memo(Home);
