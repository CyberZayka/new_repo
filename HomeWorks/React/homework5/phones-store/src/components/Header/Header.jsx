import React, {memo} from "react";
import Icon from "../Icon/Icon";
import "./Header.scss";
import HeaderLink from "./HeaderLink";

const Header = () => {
  const phoneIcon = <Icon type="phone" classes="svg-inline--fa fa-mobile-alt fa-w-10 phone-size" color="white" />

  return (
    <header className="header">
      <div className="header__logo">
        {phoneIcon}
        <span>Mob1Lo4k1</span>
      </div>
      <HeaderLink />
    </header>
  );
}

export default memo(Header);
