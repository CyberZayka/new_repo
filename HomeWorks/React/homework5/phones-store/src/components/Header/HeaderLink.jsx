import React, {memo} from "react";
import {Link} from "react-router-dom"
import "./Header.scss";

const HeaderLink = () => {
  return (
    <nav className="header__nav">
        <ul className="header__link-list">
            <li><Link to="/" className="header__link">Home</Link></li>
            <li><Link to="/favorites" className="header__link">Favorites</Link></li>
            <li><Link to="/cart" className="header__link">Cart</Link></li>
        </ul>
    </nav>
  );
}

export default memo(HeaderLink);