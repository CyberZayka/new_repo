import React, {memo} from "react";
import {Switch, Route} from "react-router-dom"

import Home from "../Pages/Home/Home"
import Favorites from "../Pages/Favorites/Favorites"
import Cart from "../Pages/Cart/Cart"
import ErrorPage from "../Pages/ErrorPage/ErrorPage"

const AppRoutes = () => {

   return (
         <>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/favorites" component={Favorites} />
                <Route path="/cart" component={Cart} />
                <Route path="*" component={ErrorPage} />
            </Switch>
         </>
   );
}

export default memo(AppRoutes);