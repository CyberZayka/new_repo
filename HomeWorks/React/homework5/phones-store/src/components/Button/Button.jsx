import React, {memo} from 'react';
import PropTypes from 'prop-types'

const Button = (props) => {
    const { onClickHandler, text, icon } = props;
    return (
        <button onClick={onClickHandler}
                className='card-add-to-btn'><span>{text}</span> {icon}</button>
    );
}

export default memo(Button);

Button.propTypes = {
    onClickHandler: PropTypes.func,
    text: PropTypes.string
};