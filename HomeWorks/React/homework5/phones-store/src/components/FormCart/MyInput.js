import React from 'react';

const MyInput = ({form, field, ...rest}) => {
  const {name} = field;

  return (
    <>
      <input {...field} {...rest}/>
      {
        form.touched[name]
        && form.errors[name]
        && <span>{form.errors[name]}</span>
      }
    </>
  );
};

export default MyInput;