import React from 'react'
import "./styles.scss"
import {useDispatch} from "react-redux";

import {Formik, Form, Field} from "formik";

import cartSchema from './validationSchema'
import MyInput from './MyInput'

const SAVE_FORM = "SAVE_FORM"

const FormCart = ({toggleModal, removeFromCart}) => {
    const dispatch = useDispatch()

    const handleSubmit = (values) => {
        dispatch({type: SAVE_FORM, payload: values})
        removeFromCart()
        toggleModal()
        alert(`Your order is: ${JSON.stringify(values)}`)
    }


    return (
        <Formik
            initialValues={{
                name: "",
                lastName: "",
                age: "",
                phone: "",
                address: ""
            }}
            validationSchema={cartSchema}
            onSubmit={handleSubmit}    
        >

            {formikProps => {
                return (
                    <div className="form-wrapper">
                        
                        <h1>Here you can order phones</h1>

                        <Form noValidate className="form-style" action="">
                            <Field component={MyInput}
                                type="text"
                                name="name"
                                placeholder="Name" 
                            />
                            <Field component={MyInput}
                                type="text"
                                name="lastName"
                                placeholder="Lastname" 
                            />
                            <Field component={MyInput}
                                type="number"
                                name="age"
                                placeholder="Age" 
                            />
                            <Field component={MyInput}
                                type="text"
                                name="phone"
                                placeholder="Phone" 
                            />
                            <Field component={MyInput}
                                type="text"
                                name="address"
                                placeholder="Address" 
                            />
                            <button type="submit" disabled={formikProps.isSubmitting}>Checkout</button>
                        </Form>
                    </div>
                )
            }}

        </Formik>
    )

}

export default FormCart