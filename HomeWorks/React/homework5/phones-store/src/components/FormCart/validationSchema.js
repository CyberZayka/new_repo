import * as yup from 'yup'

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

const cartSchema = yup.object().shape({
    name: yup
        .string()
        .required("This field is required"),
    lastName: yup
        .string()
        .required("This field is required"),
    age: yup
        .number()
        .required("This field is required"),
    phone: yup
        .string()
        .matches(phoneRegExp, "Phone number is not valid")
        .required("This field is required"),
    address: yup
        .string()
        .required("This field is required"),    
})

export default cartSchema