import React from 'react';
import {Switch, Route} from 'react-router-dom';

import Cart from "../Cart";
import Saved from "../Saved";
import Home from "../Home";
import NoMatch from "../404";

const AppRoutes = () => {

    return (
        <Switch>
            <Route exact path="/cart" component={Cart} />
            <Route exact path="/saved" component={Saved} />
            <Route exact path="/" component={Home} />
            <Route path="*">
                <NoMatch />
            </Route>
        </Switch>
    )
}

export default AppRoutes;