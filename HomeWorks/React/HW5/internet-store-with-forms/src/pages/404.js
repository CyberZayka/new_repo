import React from 'react'
import {useLocation} from 'react-router-dom';

const NoMatch = () => {

    let location = useLocation();

    return (
        <div className="jumbotron">
            <h1>
                Error 404
                Page not found
            </h1>
            <h3>
                No match for <code>{location.pathname}</code>
            </h3>
        </div>
    );

}

export default NoMatch;