import React,{useState, useEffect} from 'react'
import {useSelector, useDispatch} from 'react-redux';

import ProductList from "../components/ProductList";
import {phonesSelectors, phonesActions} from "../redux/phones";


const Home = () => {
    const dispatch = useDispatch();
    const allPhones = useSelector(phonesSelectors.allPhones)
    const {phones} = allPhones;

    const [addToCartBtnText] = useState('Add to the cart');
    const [addToCartBtnClass] = useState('add-button');

    const modalTitle = "Saving a product";
    const modalBody = "Are you sure you want to add to the cart this product?";

    //hook to save all phones in redux store with async
    useEffect(() => {
        dispatch(phonesActions.asyncSaveAllPhones())
    }, [dispatch])

/*
    //todo how i should make it more secure without global scope? (use effect dependency does not work)
    let renderPhones;

    if (phones) renderPhones = phones.map(phone => {

        return <ProductList
            {...phone}
            cartBtnText={addToCartBtnText}
            cartBtnClass={addToCartBtnClass}
            modalTitle={modalTitle}
            modalBody={modalBody}
            isInHome
        />
    })

    return (
        <>
            <ul className="flex-wrap">
                {renderPhones}
            </ul>
        </>
    )*/

    return (
        <ul className="flex-wrap">
            {
                <ProductList
                    phones={phones}
                    cartBtnText={addToCartBtnText}
                    cartBtnClass={addToCartBtnClass}
                    modalTitle={modalTitle}
                    modalBody={modalBody}
                    isInHome
                />
            }
        </ul>
    )
}


export default Home;