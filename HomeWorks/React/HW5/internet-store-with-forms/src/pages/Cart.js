import React, {useEffect, useState} from 'react';
import {useDispatch,useSelector} from 'react-redux';

import ProductList from "../components/ProductList";
import NoItems from "../components/NoItems";
import BuyAllComponent from "../components/BuyAllComponent";
import FormToOrder from "../components/Form/Form";
import {phonesActions, phonesSelectors} from "../redux/phones";

const Cart = () => {
    const dispatch = useDispatch();
    const allPhones = useSelector(phonesSelectors.allPhones)
    const {phones} = allPhones;

    const [allBoughtPhonesId, setAllBoughtPhonesId] = useState([]);
    const [deleteFromCartBtnText] = useState('Delete from the cart');
    const [deleteFromCartBtnClass] = useState('delete-button');
    const [isEmptyCart, setIsEmptyCart] = useState(false);
    const [isBoughtPhone, setIsBoughtPhone] = useState(false);

    const modalTitle = "Deleting a product";
    const modalBody = "Are you sure you want to delete from the cart this product?";

    const handleClickBuyAll = () => {
        setIsBoughtPhone(!isBoughtPhone)
    }

    const handleBoughtId = () => {
        const boughtItemsArray = localStorage.getItem('bought')

        if (boughtItemsArray) {
            const parsedArray = JSON.parse(boughtItemsArray)
            setAllBoughtPhonesId(parsedArray)
            if (parsedArray.length === 0) {
                localStorage.removeItem('bought');
            }
        } else {
            setIsEmptyCart(true)
        }
    }

    useEffect(() => {
        dispatch(phonesActions.asyncSaveAllPhones())
    }, [dispatch])

    useEffect(() => {
        handleBoughtId()
    },[])

    const renderPhones = allBoughtPhonesId.map(phoneId => {
        let productList;

        if (phones) phones.map(phone => {
            if (phone.product_id === phoneId) productList = <ProductList
                {...phone}
                cartBtnText={deleteFromCartBtnText}
                cartBtnClass={deleteFromCartBtnClass}
                modalTitle={modalTitle}
                modalBody={modalBody}
                isInCart
            />
            return phones;
        })

        return productList;
    })

    const renderProductList =
        <div>
            {<BuyAllComponent handleClickBuyAll={handleClickBuyAll} />}
            <ul className="flex-wrap">
                {renderPhones}
            </ul>
        </div>


    return (
        <>
            {isEmptyCart ? <NoItems /> : renderProductList}
            {isBoughtPhone && <FormToOrder />}
        </>
    )

}


export default Cart;