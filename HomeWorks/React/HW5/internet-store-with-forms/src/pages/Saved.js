import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';

import ProductList from "../components/ProductList";
import NoItems from "../components/NoItems";
import {phonesActions, phonesSelectors} from "../redux/phones";

const Saved = () => {
    const dispatch = useDispatch();
    const allPhones = useSelector(phonesSelectors.allPhones)
    const {phones} = allPhones;

    const [allSavedPhonesId, setAllSavedPhonesId] = useState([]);
    const [addToCartBtnText] = useState('Add to the cart');
    const [addToCartBtnClass] = useState('add-button');
    const [isEmptyFav, setIsEmptyFav] = useState(false);

    const modalTitle = "Saving a product";
    const modalBody = "Are you sure you want to add to the cart this product?";

    const handleFavoriteId = () => {
        const savedItemsArray = localStorage.getItem('favorite')

        if (savedItemsArray) {
            const parsedArray = JSON.parse(savedItemsArray)
            setAllSavedPhonesId(parsedArray)
            if (parsedArray.length === 0) {
                localStorage.removeItem('favorite');
            }
        } else {
            setIsEmptyFav(true)
        }
    }

    useEffect(() => {
        dispatch(phonesActions.asyncSaveAllPhones())
    }, [dispatch])

    useEffect(() => {
        handleFavoriteId()
    }, [])


    const renderPhones = allSavedPhonesId.map(phoneId => {

        let productList;

        if (phones) phones.map(phone => {
            if (phone.product_id === phoneId) productList = <ProductList
                {...phone}
                cartBtnText={addToCartBtnText}
                cartBtnClass={addToCartBtnClass}
                modalTitle={modalTitle}
                modalBody={modalBody}
                isInHome
            />
            return phones;
        })

        return productList;
    })


    const renderProductList =
        <ul className="flex-wrap">
            {renderPhones}
        </ul>

    return (
        <>
            {isEmptyFav ? <NoItems /> : renderProductList}
        </>
    )

}


export default Saved;