import React from 'react';
import Color from "./Color";
import Price from "./Price";
import Button from "./Button";

const Phone = (props) => {
    const {
        //model,
        //product_id,
        //color,
        //price,
        //avatar,
        cartBtnClass,
        cartBtnText,
        favIcon,
        addCartIcon,
        delCartIcon,
        getCartProductId,
        isInHome,
        isInCart,
        keyId,
        phone
    } = props;

    const {model, product_id, color, price, avatar} = phone;

    return (
        <li className="li flex card-box flex-card" key={keyId}>
            <div className="flex-column">
                <div className="flex-elem">
                    <h2>
                        {model}
                    </h2>
                    {isInHome && favIcon}
                </div>
                <h5>
                    {`Product id: ${product_id}`}
                </h5>
                <Color
                    color={color}
                />
                <Price
                    price={price}
                />
                <img src={avatar} alt='phone' />
                <div className="flex-center">
                    <Button
                        classBtn={cartBtnClass}
                        textBtn={cartBtnText}
                        icon={
                                isInCart ? delCartIcon : addCartIcon
                            }
                        action={getCartProductId}
                    />
                </div>
            </div>
        </li>
    );
};

export default Phone;