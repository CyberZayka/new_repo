import React from 'react'
import Button from "./Button";

const Modal = (props) => {
    const {title, body, showHideModal, saveOrDeleteCart} = props;

    const agreeBtn = "Yes";
    const disagreeBtn = "No";
    const closeBtn = <span aria-hidden="true">&times;</span>;

    return (
        <div className="modal" tabIndex="-1" style={{display:"block"}}>
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">
                            {title}
                        </h5>
                        <Button
                            classBtn="close"
                            textBtn={closeBtn}
                            action={showHideModal}
                        />
                    </div>
                    <div className="modal-body">
                        <p>
                            {body}
                        </p>
                    </div>
                    <div className="modal-footer">
                        <Button
                            classBtn="btn btn-primary"
                            textBtn={agreeBtn}
                            action={saveOrDeleteCart}
                        />
                        <Button
                            classBtn="btn btn-secondary"
                            textBtn={disagreeBtn}
                            action={showHideModal}
                        />
                    </div>
                </div>
            </div>
        </div>
    )

}

export default Modal;