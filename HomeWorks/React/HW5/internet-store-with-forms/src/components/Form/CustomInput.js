import React from "react";

const CustomInput = (props) => {
    const {field, form, label, ...rest} = props;
    const {name} = field;

    return (
        <>
            <div>
                <label>{label}
                    <input {...field} {...rest} />
                </label>
            </div>
            {
                form.touched[name] && form.errors[name] &&
                <div style={{color: 'red'}}>{form.errors[name]}</div>
            }
        </>
    )

}

export default CustomInput;