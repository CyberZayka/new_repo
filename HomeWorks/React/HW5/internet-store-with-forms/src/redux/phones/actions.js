import types from "./types";

const saveAllPhonesAction = allPhones => ({
    type: types.SAVE__ALL_PHONES,
    payload: allPhones
})

const isOpenModalAction = isOpenModal => ({
    type: types.IS_OPEN_MODAL,
    payload: !isOpenModal
})

const cartIdAction = cartId => ({
    type: types.CART_ID,
    payload: cartId
})


const asyncSaveAllPhones = () => {
    return dispatch => {
        fetch('/phones.json')
            .then(response => response.json())
            .then(data => {
                dispatch(saveAllPhonesAction(data))
            })
    }
}

export default {
    asyncSaveAllPhones: asyncSaveAllPhones,
    isOpenModal: isOpenModalAction,
    setCartId: cartIdAction
}