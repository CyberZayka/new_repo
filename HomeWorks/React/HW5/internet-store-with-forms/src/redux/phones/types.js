const SAVE__ALL_PHONES = "SAVE_ALL_PHONES";
const IS_OPEN_MODAL = "IS_OPEN_MODAL";
const CART_ID = "CART_ID";

export default {
    SAVE__ALL_PHONES,
    IS_OPEN_MODAL,
    CART_ID
}