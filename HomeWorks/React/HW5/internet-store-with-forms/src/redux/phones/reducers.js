import types from "./types";

const initialStore = {
    allPhones: [],
    isOpenModal: false,
    cartId: []
}


const phoneReducer = (currentStore = initialStore, action) => {
    switch (action.type) {
        case types.SAVE__ALL_PHONES:
            return {
                allPhones: action.payload
            }
        case types.IS_OPEN_MODAL:
            return {
                ...currentStore,
                isOpenModal: action.payload
            }
        case types.CART_ID:

            return {
                ...currentStore,
                cartId: action.payload
            }
        default:
            return currentStore;
    }
}

export default phoneReducer;