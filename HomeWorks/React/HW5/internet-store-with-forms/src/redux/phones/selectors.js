
const selectAllPhones = currentStore => currentStore.phones.allPhones;
const selectPriceColor = currentStore => currentStore.phones.priceColor;
const selectIsOpenModal = currentStore => currentStore.phones.isOpenModal;
const selectCartId = currentStore => currentStore.phones.cartId;

export default {
    allPhones: selectAllPhones,
    priceColor: selectPriceColor,
    isOpenModal: selectIsOpenModal,
    cartId: selectCartId
}