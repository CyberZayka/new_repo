import {combineReducers} from "redux";
import phones from "./phones/index"

export default combineReducers({
    phones
})