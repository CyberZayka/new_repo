import {Link} from "react-router-dom";
import React from "react";

const Dashboard = () => {
    return (
        <header>
            <nav>
                <ul>
                    <li><Link to='/home'>Home</Link></li>
                    <li><Link to='/team'>Team</Link></li>
                    <li><Link to='/schedule'>Schedule</Link></li>
                </ul>
            </nav>
        </header>
    )
}

export default Dashboard;